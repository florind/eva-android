package com.sianadev.car_alerts.main.service;

import java.util.Date;

import androidx.room.TypeConverter;

public class DateTypeConverter {
    @TypeConverter
    public static Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value * 1000);
    }

    @TypeConverter
    public static Long dateToTimestamp(Date date) {
        return date == null ? null : date.getTime() / 1000;
    }
}
