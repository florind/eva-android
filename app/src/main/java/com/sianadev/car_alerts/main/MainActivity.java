package com.sianadev.car_alerts.main;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.sianadev.car_alerts.R;
import com.sianadev.car_alerts.main.fragment.SettingsFragment;
import com.sianadev.car_alerts.reminder.fragment.NotificationListFragment;
import com.sianadev.car_alerts.reminder.service.BackgroundWorkService;
import com.sianadev.car_alerts.vehicle.VehicleFormActivity;
import com.sianadev.car_alerts.vehicle.fragment.VehicleListFragment;
import com.sianadev.car_alerts.vehicle.model.VehicleViewModel;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.preference.PreferenceManager;
import androidx.viewpager.widget.ViewPager;

public class MainActivity extends AppCompatActivity implements PurchasesUpdatedListener {
    private static final int VEHICLE_LIST_FRAGMENT = 0;
    private static final int NOTIFICATION_LIST_FRAGMENT = 1;
    private FloatingActionButton addVehicleButton;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);

        addVehicleButton = findViewById(R.id.add_vehicle);
        TabLayout tabLayout = findViewById(R.id.tab_layout);
        viewPager = findViewById(R.id.view_pager);

        viewPager.setAdapter(new PagerAdapter(getSupportFragmentManager()));
        addVehicleButton.setOnClickListener(addVehicleOnClickListener);
        viewPager.addOnPageChangeListener(onPageChangeListener);
        tabLayout.setupWithViewPager(viewPager);

        registerBackgroundWork();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.app_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() > 0) {
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
            return;
        }

        super.onBackPressed();
    }

    private void registerBackgroundWork() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        if (!sharedPreferences.contains(SettingsFragment.NOTIFICATIONS_KEY)) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(SettingsFragment.NOTIFICATIONS_KEY, true);
            editor.apply();
        }

        boolean notificationsEnabled = sharedPreferences.getBoolean(SettingsFragment.NOTIFICATIONS_KEY, false);

        if (notificationsEnabled) {
            BackgroundWorkService.register(getApplicationContext());
        }
    }

    private View.OnClickListener addVehicleOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(getApplicationContext(), VehicleFormActivity.class);
            startActivity(intent);
        }
    };

    private ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

        @Override
        public void onPageSelected(int position) {
            switch (position) {
                case 0:
                    addVehicleButton.show();
                    break;
                case 1:
                    addVehicleButton.hide();
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {}
    };

    class PagerAdapter extends FragmentPagerAdapter {
        PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case VEHICLE_LIST_FRAGMENT:
                    VehicleListFragment fragment = new VehicleListFragment();
                    if (getIntent().getExtras() != null) {
                        fragment.setArguments(getIntent().getExtras());
                    }

                    return fragment;
                case NOTIFICATION_LIST_FRAGMENT:
                    return new NotificationListFragment();
            }
            return null;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case VEHICLE_LIST_FRAGMENT:
                    return getString(R.string.nav_menu_vehicles);
                case NOTIFICATION_LIST_FRAGMENT:
                    return getString(R.string.nav_menu_reminders);
            }
            return super.getPageTitle(position);
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    @Override
    public void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases) {
        switch (responseCode) {
            case BillingClient.BillingResponse.OK:
                Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_SHORT).show();
                break;
            case BillingClient.BillingResponse.USER_CANCELED:
                Toast.makeText(getApplicationContext(), "Canceled", Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
        }
    }
}
