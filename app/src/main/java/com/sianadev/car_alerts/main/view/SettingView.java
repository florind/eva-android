package com.sianadev.car_alerts.main.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sianadev.car_alerts.R;

import androidx.annotation.Nullable;

public class SettingView extends LinearLayout {
    private TextView title, description, error;
    private RelativeLayout wrapper;

    public SettingView(Context context) {
        super(context);
        init();
    }

    public SettingView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SettingView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.O)
    public SettingView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.setting_layout, this);
        wrapper = findViewById(R.id.wrapper);
        title = findViewById(R.id.title);
        description = findViewById(R.id.description);
        error = findViewById(R.id.error);
    }

    public void enable() {
        title.setEnabled(true);
        description.setEnabled(true);
        wrapper.setEnabled(true);
        clearError();
    }

    public void disable() {
        title.setEnabled(false);
        description.setEnabled(false);
        wrapper.setEnabled(false);
        clearError();
    }

    public void setTitle(String text) {
        title.setText(text);
        clearError();
    }

    public void setDescription(String text) {
        if (text == null || text.trim().isEmpty()) {
            return;
        }

        description.setText(text);
        clearError();
    }

    public void setError(String text) {
        error.setText(text);
        error.setVisibility(VISIBLE);
    }

    public void clearError() {
        error.setText("");
        error.setVisibility(GONE);
    }

    public void setListener(OnClickListener listener) {
        wrapper.setOnClickListener(listener);
    }
}
