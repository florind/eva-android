package com.sianadev.car_alerts.main.service;

import android.content.Context;

import com.sianadev.car_alerts.reminder.dao.ReminderDao;
import com.sianadev.car_alerts.reminder.entity.Reminder;
import com.sianadev.car_alerts.vehicle.dao.VehicleDao;
import com.sianadev.car_alerts.vehicle.entity.Vehicle;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@Database(entities = {Vehicle.class, Reminder.class}, version = 14)
@TypeConverters({DateTypeConverter.class})
public abstract class DatabaseService extends RoomDatabase {
    public abstract VehicleDao vehicleDao();
    public abstract ReminderDao alarmDao();

    private static volatile DatabaseService INSTANCE;

    public static DatabaseService instance(final Context context) {
        if (INSTANCE == null) {
            synchronized (DatabaseService.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(
                            context.getApplicationContext(),
                            DatabaseService.class,
                            "sianadev_eva_db"
                    ).fallbackToDestructiveMigration().build();
                }
            }
        }

        return INSTANCE;
    }
}


