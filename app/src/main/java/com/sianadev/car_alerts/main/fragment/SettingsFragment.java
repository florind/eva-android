package com.sianadev.car_alerts.main.fragment;

import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import com.sianadev.car_alerts.R;
import com.sianadev.car_alerts.reminder.service.BackgroundWorkService;

import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceScreen;
import androidx.preference.SwitchPreferenceCompat;

public class SettingsFragment extends PreferenceFragmentCompat {
    public static final String NOTIFICATIONS_KEY = "notifications";

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        Context context = getPreferenceManager().getContext();
        PreferenceScreen screen = getPreferenceManager().createPreferenceScreen(context);

        SwitchPreferenceCompat notification = new SwitchPreferenceCompat(context);
        notification.setKey(NOTIFICATIONS_KEY);
        notification.setTitle(getString(R.string.pref_notifications_title));
        notification.setSummary(getString(R.string.pref_notifications_summary));
        notification.setOnPreferenceChangeListener(notificationsPreferenceChangeListener);

        screen.addPreference(notification);
        setPreferenceScreen(screen);
    }

    private Preference.OnPreferenceChangeListener notificationsPreferenceChangeListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            if ((boolean) value) {
                BackgroundWorkService.register(getContext());
                Toast.makeText(getContext(), getString(R.string.pref_notifications_on_toast), Toast.LENGTH_SHORT).show();
            } else {
                BackgroundWorkService.unregister();
                Toast.makeText(getContext(), getString(R.string.pref_notifications_off_toast), Toast.LENGTH_SHORT).show();
            }

            return true;
        }
    };
}
