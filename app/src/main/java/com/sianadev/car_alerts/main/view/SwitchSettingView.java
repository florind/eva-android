package com.sianadev.car_alerts.main.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.sianadev.car_alerts.R;

import androidx.annotation.Nullable;

public class SwitchSettingView extends LinearLayout {

    Switch toggle;
    TextView description;

    public SwitchSettingView(Context context) {
        super(context);
        init();
    }

    public SwitchSettingView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SwitchSettingView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.O)
    public SwitchSettingView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    @Override
    public void setEnabled(boolean enabled) {
        toggle.setEnabled(enabled);
        super.setEnabled(enabled);
    }

    private void init() {
        inflate(getContext(), R.layout.switch_setting_layout, this);

        toggle = findViewById(R.id.toggle);
        description = findViewById(R.id.description);

        description.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle.toggle();
            }
        });
    }

    public void check() {
        toggle.setChecked(true);
    }

    public void uncheck() {
        toggle.setChecked(false);
    }

    public boolean isChecked() {
        return toggle.isChecked();
    }

    public void setTitle(String text) {
        toggle.setText(text);
    }

    public void setDescription(String text) {
        if (text.trim().isEmpty()) {
            return;
        }
        description.setText(text);
        description.setVisibility(VISIBLE);
    }

    public void clearDescription() {
        description.setVisibility(GONE);
    }

    public void setListener(CompoundButton.OnCheckedChangeListener listener) {
        toggle.setOnCheckedChangeListener(listener);
    }
}
