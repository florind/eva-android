package com.sianadev.car_alerts.vehicle.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.sianadev.car_alerts.R;
import com.sianadev.car_alerts.vehicle.entity.Vehicle;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class VehicleListAdapter extends RecyclerView.Adapter<VehicleListAdapter.ViewHolder> {

    private List<Vehicle> vehicles;
    private final OnListFragmentInteractionListener listener;

    public VehicleListAdapter(OnListFragmentInteractionListener listener) {
        this.listener = listener;
        vehicles = new ArrayList<>();
    }

    public void setVehicles(List<Vehicle> vehicles) {
        this.vehicles = vehicles;
        notifyDataSetChanged();
    }

    public Vehicle getVehicleWithId(int id) {
        for (Vehicle vehicle : vehicles) {
            if (vehicle.getId() == id) {
                return vehicle;
            }
        }

        return null;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.vehicle_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.vehicle = vehicles.get(position);
        holder.model.setText(holder.vehicle.getModel());

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != listener) {
                    listener.onListItemClick(holder.vehicle);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return vehicles.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View view;
        public final TextView model;
        public final ImageButton options;
        public Vehicle vehicle;

        ViewHolder(View view) {
            super(view);
            this.view = view;
            model = view.findViewById(R.id.model);
            options = view.findViewById(R.id.options);

            options.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onContextMenuClick(view, vehicle);
                }
            });
        }
    }

    public interface OnListFragmentInteractionListener {
        void onListItemClick(Vehicle vehicle);
        void onContextMenuClick(View view, Vehicle vehicle);
    }
}
