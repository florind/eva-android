package com.sianadev.car_alerts.vehicle.model;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import com.sianadev.car_alerts.vehicle.entity.Vehicle;
import com.sianadev.car_alerts.vehicle.repository.VehicleRepository;

import java.util.List;

public class VehicleViewModel extends AndroidViewModel {
    private VehicleRepository repository;
    private LiveData<List<Vehicle>> list;

    public VehicleViewModel(@NonNull Application application) {
        super(application);
        repository = new VehicleRepository(application);
        list = repository.list();
    }

    public LiveData<List<Vehicle>> list() {
        return list;
    }

    public LiveData<Integer> count() {
        return repository.count();
    }

    public LiveData<Vehicle> get(int id) {
        return repository.get(id);
    }
}
