package com.sianadev.car_alerts.vehicle.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.textfield.TextInputEditText;
import com.sianadev.car_alerts.R;
import com.sianadev.car_alerts.vehicle.entity.Vehicle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public class MileageDialogFragment extends DialogFragment {
    private Vehicle vehicle;
    private Listener listener;

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View layout = getActivity().getLayoutInflater().inflate(R.layout.mileage_form, null);

        final TextInputEditText mileageInput = layout.findViewById(R.id.mileage);
        mileageInput.setText(String.valueOf(vehicle.getMileage()));

        final AlertDialog dialog = builder.setView(layout)
                .setTitle(getString(R.string.mileage_update_form_title, vehicle.getModel()))
                .setPositiveButton(R.string.action_save, null)
                .setNegativeButton(R.string.action_cancel, null)
                .create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialogInterface) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String mileage = mileageInput.getText().toString().trim();
                                if (mileage.isEmpty()) {
                                    mileageInput.setError(getString(R.string.error_required));
                                    return;
                                }

                                if (Integer.parseInt(mileage) < vehicle.getMileage()) {
                                    mileageInput.setError(getString(R.string.error_mileage_too_low));
                                    return;
                                }

                                vehicle.setMileage(mileage);

                                listener.onMileageUpdated(vehicle);
                                dialogInterface.dismiss();
                            }
                        });
            }
        });

        return dialog;
    }

    public interface Listener {
        void onMileageUpdated(Vehicle vehicle);
    }
}
