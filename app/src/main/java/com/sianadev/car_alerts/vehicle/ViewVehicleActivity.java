package com.sianadev.car_alerts.vehicle;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.sianadev.car_alerts.R;
import com.sianadev.car_alerts.reminder.fragment.ReminderListFragment;
import com.sianadev.car_alerts.vehicle.entity.Vehicle;
import com.sianadev.car_alerts.vehicle.model.VehicleViewModel;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

public class ViewVehicleActivity extends AppCompatActivity {

    private Vehicle vehicle;
    private TextView model;
    private TextView details;
    private TextView mileage;
    private View reminderBottomSheet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_vehicle_view);

        model = findViewById(R.id.model);
        details = findViewById(R.id.details);
        mileage = findViewById(R.id.mileage);
        reminderBottomSheet = findViewById(R.id.alarm_list_fragment);

        reminderBottomSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(view);

                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }

                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            }
        });

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            int vehicleId = extras.getInt("VEHICLE_ID");
            VehicleViewModel vehicleViewModel = ViewModelProviders.of(this).get(VehicleViewModel.class);
            vehicleViewModel.get(vehicleId).observe(this, vehicleObserver);
        }
    }

    @Override
    public void onBackPressed() {
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(reminderBottomSheet);

        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            return;
        }

        super.onBackPressed();
    }

    private Observer<Vehicle> vehicleObserver = new Observer<Vehicle>() {
        @Override
        public void onChanged(@Nullable Vehicle result) {
            if (result == null) {
                return;
            }
            vehicle = result;
            model.setText(vehicle.getModel());
            details.setText(vehicle.getDetails());
            mileage.setText(vehicle.getMileageString());

            //render upcomingAlarm list
            ReminderListFragment alarmList = new ReminderListFragment();
            Bundle args = new Bundle();
            args.putSerializable("VEHICLE", vehicle);
            alarmList.setArguments(args);

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.alarm_list_fragment, alarmList);
            transaction.commit();
        }
    };
}
