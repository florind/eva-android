package com.sianadev.car_alerts.vehicle.entity;

import java.io.Serializable;
import java.util.Date;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "vehicles")
public class Vehicle implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String model;
    private String details;
    private int mileage;
    private Date nextMileageReadDate;

    public Vehicle() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public String getDetails() {
        return details;
    }

    public int getMileage() {
        return mileage;
    }

    public String getMileageString() {
        return String.valueOf(mileage).concat(" km");
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = Integer.parseInt(mileage);
    }

    public Date getNextMileageReadDate() {
        return nextMileageReadDate;
    }

    public void setNextMileageReadDate(Date nextMileageReadDate) {
        this.nextMileageReadDate = nextMileageReadDate;
    }
}
