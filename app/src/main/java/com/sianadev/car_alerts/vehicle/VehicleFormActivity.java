package com.sianadev.car_alerts.vehicle;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.textfield.TextInputEditText;
import com.sianadev.car_alerts.R;
import com.sianadev.car_alerts.vehicle.entity.Vehicle;
import com.sianadev.car_alerts.vehicle.model.VehicleViewModel;
import com.sianadev.car_alerts.vehicle.repository.VehicleRepository;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

public class VehicleFormActivity extends AppCompatActivity {

    private TextInputEditText modelInput;
    private TextInputEditText detailsInput;
    private TextInputEditText mileageInput;
    private VehicleRepository repository;
    private Vehicle vehicle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_form);
        repository = new VehicleRepository(getApplicationContext());

        modelInput = findViewById(R.id.model);
        detailsInput = findViewById(R.id.details);
        mileageInput = findViewById(R.id.mileage);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            int vehicleId = extras.getInt("VEHICLE_ID");

            VehicleViewModel vehicleList = ViewModelProviders.of(this).get(VehicleViewModel.class);
            vehicleList.get(vehicleId).observe(this, new Observer<Vehicle>() {
                @Override
                public void onChanged(@Nullable Vehicle result) {
                    if (result != null) {
                        vehicle = result;
                        modelInput.setText(vehicle.getModel());
                        detailsInput.setText(vehicle.getDetails());
                        mileageInput.setText(String.valueOf(vehicle.getMileage()));
                    }
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        if (item.getItemId() == R.id.action_save) {
            if (formIsValid()) {
                String model = modelInput.getText().toString();
                String details = detailsInput.getText().toString();
                String mileage = mileageInput.getText().toString();

                vehicle = vehicle == null ? new Vehicle() : vehicle;

                vehicle.setModel(model);
                vehicle.setMileage(mileage);
                vehicle.setDetails(details);
                vehicle.setNextMileageReadDate(generateRandomMileageReadDate());

                repository.save(vehicle);

                finish();
            }

        }

        return super.onOptionsItemSelected(item);
    }

    private boolean formIsValid() {
        boolean isValid = true;

        if (modelInput.getText() == null || modelInput.getText().toString().trim().isEmpty()) {
            modelInput.setError(getString(R.string.error_required));
            isValid = false;
        }

        if (mileageInput.getText() == null || mileageInput.getText().toString().trim().isEmpty()) {
            mileageInput.setError(getString(R.string.error_required));
            isValid = false;
        }

        return isValid;
    }

    private Date generateRandomMileageReadDate() {
        int days = getResources().getInteger(R.integer.conf_mileage_read_days);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 1);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.add(Calendar.DAY_OF_MONTH, new Random().nextInt(days) + days + 1);

        return calendar.getTime();
    }
}
