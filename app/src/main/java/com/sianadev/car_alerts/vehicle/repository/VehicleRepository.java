package com.sianadev.car_alerts.vehicle.repository;

import android.content.Context;
import android.os.AsyncTask;

import com.sianadev.car_alerts.main.service.DatabaseService;
import com.sianadev.car_alerts.vehicle.dao.VehicleDao;
import com.sianadev.car_alerts.vehicle.entity.Vehicle;

import java.util.List;

import androidx.lifecycle.LiveData;

public class VehicleRepository {
    private VehicleDao dao;

    public VehicleRepository(Context context) {
        DatabaseService db = DatabaseService.instance(context);
        dao = db.vehicleDao();
    }

    public LiveData<List<Vehicle>> list() {
        return dao.list();
    }

    public LiveData<Integer> count() {
        return dao.count();
    }

    public LiveData<Vehicle> get(int id) {
        return dao.get(id);
    }

    public void save(Vehicle vehicle) {
        new saveAsyncTask(dao).execute(vehicle);
    }

    public void delete(Vehicle vehicle) {
        new deleteAsyncTask(dao).execute(vehicle);
    }

    private static class saveAsyncTask extends AsyncTask<Vehicle, Void, Void> {
        private VehicleDao dao;

        saveAsyncTask(VehicleDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(Vehicle... vehicles) {
            dao.save(vehicles[0]);
            return null;
        }
    }

    private static class deleteAsyncTask extends AsyncTask<Vehicle, Void, Void> {
        private VehicleDao dao;

        deleteAsyncTask(VehicleDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(Vehicle... vehicles) {
            dao.delete(vehicles[0]);
            return null;
        }
    }
}
