package com.sianadev.car_alerts.vehicle.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;

import com.sianadev.car_alerts.R;
import com.sianadev.car_alerts.reminder.repository.ReminderRepository;
import com.sianadev.car_alerts.vehicle.VehicleFormActivity;
import com.sianadev.car_alerts.vehicle.ViewVehicleActivity;
import com.sianadev.car_alerts.vehicle.adapter.VehicleListAdapter;
import com.sianadev.car_alerts.vehicle.entity.Vehicle;
import com.sianadev.car_alerts.vehicle.model.VehicleViewModel;
import com.sianadev.car_alerts.vehicle.repository.VehicleRepository;

import java.util.Calendar;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class VehicleListFragment extends Fragment implements
        VehicleListAdapter.OnListFragmentInteractionListener,
        MileageDialogFragment.Listener{

    private VehicleListAdapter adapter;
    private ReminderRepository alarmRepository;
    private VehicleRepository vehicleRepository;

    public VehicleListFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = new VehicleListAdapter(this);

        VehicleViewModel vehicleList = ViewModelProviders.of(getActivity()).get(VehicleViewModel.class);
        vehicleList.list().observe(this, new Observer<List<Vehicle>>() {
            @Override
            public void onChanged(@Nullable List<Vehicle> vehicles) {
                adapter.setVehicles(vehicles);
                handleNotificationMileageReadIntent();
            }
        });
    }

    private void handleNotificationMileageReadIntent() {
        Bundle bundle = getArguments();

        if (bundle == null) {
            return;
        }

        String action = bundle.getString("ACTION", null);
        if (action == null || !action.equals("MILEAGE_READ")) {
            return;
        }

        int vehicleId = bundle.getInt("VEHICLE_ID");
        if (vehicleId == 0) {
            return;
        }

        Vehicle vehicle = adapter.getVehicleWithId(vehicleId);

        if (vehicle == null) {
            return;
        }

        MileageDialogFragment dialog = new MileageDialogFragment();
        dialog.setListener(VehicleListFragment.this);
        dialog.setVehicle(vehicle);
        dialog.show(getFragmentManager(), dialog.getClass().toString());

        setArguments(null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vehicle_list, container, false);

        // Set the adapter
        if (view.findViewById(R.id.list) instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = view.findViewById(R.id.list);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(adapter);
        }

        return view;
    }

    @Override
    public void onListItemClick(Vehicle vehicle) {
        Intent intent = new Intent(getContext(), ViewVehicleActivity.class);
        intent.putExtra("VEHICLE_ID", vehicle.getId());
        startActivity(intent);
    }

    @Override
    public void onContextMenuClick(View view, final Vehicle vehicle) {
        PopupMenu menu = new PopupMenu(getContext(), view);
        menu.getMenuInflater().inflate(R.menu.vehicle_context_menu, menu.getMenu());
        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.mileage:
                        MileageDialogFragment dialog = new MileageDialogFragment();
                        dialog.setListener(VehicleListFragment.this);
                        dialog.setVehicle(vehicle);
                        dialog.show(getFragmentManager(), dialog.getClass().toString());
                        break;
                    case R.id.edit:
                        Intent intent = new Intent(getContext(), VehicleFormActivity.class);
                        intent.putExtra("VEHICLE_ID", vehicle.getId());
                        startActivity(intent);
                        break;
                    case R.id.delete:
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage(getString(R.string.confirm_delete_message, vehicle.getModel()))
                                .setPositiveButton(getString(R.string.action_delete), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        getAlarmRepository().flush(vehicle.getId());
                                        getVehicleRepository().delete(vehicle);
                                    }
                                })
                                .setNegativeButton(getString(R.string.action_cancel), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                }).create().show();
                        break;
                }

                return true;
            }
        });
        menu.show();
    }

    @Override
    public void onMileageUpdated(Vehicle vehicle) {
        int days = getResources().getInteger(R.integer.conf_mileage_read_days);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 1);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.add(Calendar.DAY_OF_MONTH, days);

        vehicle.setNextMileageReadDate(calendar.getTime());
        getVehicleRepository().save(vehicle);
    }

    private ReminderRepository getAlarmRepository() {
        if (alarmRepository == null) {
            alarmRepository = new ReminderRepository(getContext());
        }

        return alarmRepository;
    }

    private VehicleRepository getVehicleRepository() {
        if (vehicleRepository == null) {
            vehicleRepository = new VehicleRepository(getContext());
        }

        return vehicleRepository;
    }
}
