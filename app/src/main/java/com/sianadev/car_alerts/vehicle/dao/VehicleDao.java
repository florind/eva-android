package com.sianadev.car_alerts.vehicle.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.sianadev.car_alerts.vehicle.entity.Vehicle;

import java.util.List;

@Dao
public interface VehicleDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void save(Vehicle vehicle);

    @Query("SELECT * FROM vehicles ORDER BY id DESC")
    LiveData<List<Vehicle>> list();

    @Query("SELECT count(*) FROM vehicles")
    LiveData<Integer> count();

    @Query("SELECT * FROM vehicles WHERE strftime(\"%s\", \"now\") >= nextMileageReadDate")
    List<Vehicle> getPendingMileageRead();

    @Query("SELECT * FROM vehicles WHERE id=:id")
    LiveData<Vehicle> get(int id);

    @Query("SELECT * FROM vehicles WHERE id=:id")
    Vehicle syncGet(int id);

    @Delete
    void delete(Vehicle... vehicles);
}
