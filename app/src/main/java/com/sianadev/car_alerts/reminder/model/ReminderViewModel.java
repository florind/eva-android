package com.sianadev.car_alerts.reminder.model;

import android.app.Application;

import com.sianadev.car_alerts.reminder.entity.Reminder;
import com.sianadev.car_alerts.reminder.entity.AlarmNotification;
import com.sianadev.car_alerts.reminder.repository.ReminderRepository;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class ReminderViewModel extends AndroidViewModel {
    private ReminderRepository repository;

    public ReminderViewModel(@NonNull Application application) {
        super(application);
        repository = new ReminderRepository(application);
    }

    public LiveData<List<Reminder>> list(int vehicleId) {
        return repository.list(vehicleId);
    }

    public LiveData<List<AlarmNotification>> getNotificationsLiveData() {
        return repository.getNotificationsLiveData();
    }

    public LiveData<Reminder> get(int id) {
        return repository.get(id);
    }
}
