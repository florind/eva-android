package com.sianadev.car_alerts.reminder;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.Toast;

import com.sianadev.car_alerts.R;
import com.sianadev.car_alerts.reminder.entity.Reminder;
import com.sianadev.car_alerts.reminder.fragment.SingleNumberInputFormDialog;
import com.sianadev.car_alerts.reminder.fragment.TimeIntervalPickerDialog;
import com.sianadev.car_alerts.reminder.helper.TimeIntervalFactory;
import com.sianadev.car_alerts.main.view.SettingView;
import com.sianadev.car_alerts.main.view.SwitchSettingView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class AlarmSettingsActivity extends AppCompatActivity
        implements TimeIntervalPickerDialog.Listener, SingleNumberInputFormDialog.Listener {

    private static final int MILEAGE_INPUT_ID = 207;
    private static final int MILEAGE_WARNING_INPUT_ID = 971;

    SwitchSettingView dateSwitch, mileageSwitch;
    SettingView date, dateWarn, mileage, mileageWarn;
    private Calendar calendar;
    private int currentMileage;
    private Reminder alarm;
    private TimeIntervalFactory intervalFactory;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alarm_settings_activity);

        alarm = new Reminder();
        calendar = Calendar.getInstance();

        intervalFactory = TimeIntervalFactory.instance();

        dateSwitch = findViewById(R.id.date_switch);
        mileageSwitch = findViewById(R.id.mileage_switch);
        date = findViewById(R.id.date);
        dateWarn = findViewById(R.id.date_warn);
        mileage = findViewById(R.id.mileage);
        mileageWarn = findViewById(R.id.mileage_warn);

        dateSwitch.setTitle(getString(R.string.reminder_settings_date_switch_title));
        dateSwitch.setDescription(getString(R.string.reminder_settings_date_switch_description));
        dateSwitch.setListener(dateSwitchOnChangeListener);

        mileageSwitch.setTitle(getString(R.string.reminder_settings_mileage_switch_title));
        mileageSwitch.setDescription(getString(R.string.reminder_settings_mileage_switch_description));
        mileageSwitch.setListener(mileageSwitchOnChangeListener);

        date.setTitle(getString(R.string.reminder_settings_date_title));
        date.setDescription(getString(R.string.not_set));
        date.disable();

        mileage.setTitle(getString(R.string.reminder_settings_mileage_title));
        mileage.setDescription(getString(R.string.not_set));
        mileage.disable();

        dateWarn.setTitle(getString(R.string.reminder_settings_date_warning_title));
        dateWarn.setDescription(intervalFactory.from(alarm.getEarlyWarningInterval()).toString());
        dateWarn.disable();

        mileageWarn.setTitle(getString(R.string.reminder_settings_mileage_warning_title));
        mileageWarn.setDescription(getString(R.string.mileage_read, String.valueOf(alarm.getEarlyWarningMileage())));
        mileageWarn.disable();

        date.setListener(datepickerListener);
        dateWarn.setListener(dateWarnOnClickListener);
        mileage.setListener(mileageOnClickListener);
        mileageWarn.setListener(mileageWarnOnClickListener);

        handleIntent();
    }

    private void handleIntent() {
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            alarm = (Reminder) bundle.getSerializable(Reminder.class.toString());

            if (alarm != null) {
                currentMileage = bundle.getInt("MILEAGE", -1);

                if (alarm.isOnDate()) {
                    dateSwitch.check();
                    date.setDescription(alarm.getDate(getString(R.string.date_format)));
                    dateWarn.setDescription(intervalFactory.from(alarm.getEarlyWarningInterval()).toString());
                }

                if (alarm.isOnMileage()) {
                    mileageSwitch.check();
                    mileage.setDescription(getString(R.string.mileage_read, String.valueOf(alarm.getMileage())));
                    mileageWarn.setDescription(getString(R.string.mileage_read, String.valueOf(alarm.getEarlyWarningMileage())));
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.ok_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent response = new Intent();
            setResult(Activity.RESULT_CANCELED, response);
            finish();
            return true;
        }

        if (item.getItemId() == R.id.action_ok) {
            if (settingsAreValid()) {
                Intent response = new Intent();
                response.putExtra(Reminder.class.toString(), alarm);
                setResult(Activity.RESULT_OK, response);
                finish();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private View.OnClickListener datepickerListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new DatePickerDialog(
                    view.getContext(),
                    R.style.Datepicker,
                    datepickerChangedListener,
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)
            ).show();
        }
    };

    private DatePickerDialog.OnDateSetListener datepickerChangedListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, day);

            SimpleDateFormat dateFormatter = new SimpleDateFormat(getString(R.string.date_format), Locale.getDefault());
            date.setDescription(dateFormatter.format(calendar.getTime()));

            alarm.setDate(calendar.getTime());
        }
    };

    private View.OnClickListener dateWarnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            TimeIntervalPickerDialog dialog = TimeIntervalPickerDialog.instance(getString(R.string.reminder_dialog_date_warn_title), 0);
            dialog.show(getSupportFragmentManager(), dialog.getClass().toString());
        }
    };

    @Override
    public void onTimeIntervalSelected(int requestCode, TimeIntervalFactory.TimeInterval interval) {
        alarm.setEarlyWarningInterval(interval.getValue());
        dateWarn.setDescription(interval.toString());
    }

    private View.OnClickListener mileageOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            SingleNumberInputFormDialog dialog = SingleNumberInputFormDialog.instance(
                    MILEAGE_INPUT_ID,
                    getString(R.string.reminder_dialog_mileage_title),
                    String.valueOf(currentMileage)
            );
            dialog.setValue(alarm.getMileage());
            dialog.show(getSupportFragmentManager(), dialog.getClass().toString());
        }
    };

    private View.OnClickListener mileageWarnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            SingleNumberInputFormDialog dialog = SingleNumberInputFormDialog.instance(
                    MILEAGE_WARNING_INPUT_ID,
                    getString(R.string.reminder_dialog_mileage_warn_title),
                    ""
            );
            dialog.setValue(alarm.getEarlyWarningMileage());
            dialog.show(getSupportFragmentManager(), dialog.getClass().toString());
        }
    };

    @Override
    public void onSubmitSingleNumberInputForm(int id, int value) {
        switch (id) {
            case MILEAGE_INPUT_ID:
                mileage.setDescription(getString(R.string.mileage_read, String.valueOf(value)));
                alarm.setMileage(value);
                break;
            case MILEAGE_WARNING_INPUT_ID:
                mileageWarn.setDescription(getString(R.string.mileage_read, String.valueOf(value)));
                alarm.setEarlyWarningMileage(value);
                break;
        }
    }

    private boolean settingsAreValid() {
        boolean valid = true;

        if (alarm.isOnDate()) {
            if (alarm.getDate() == null) {
                date.setError(getString(R.string.not_set_error));
                valid = false;
            }

            if (alarm.getDate() != null && alarm.getDate().getTime() < Calendar.getInstance().getTimeInMillis()) {
                date.setError(getString(R.string.error_date_in_past));
                valid = false;
            }
        }

        if (alarm.isOnMileage()) {
            if (alarm.getMileage() == 0) {
                mileage.setError(getString(R.string.not_set_error));
                valid = false;
            }

            if (alarm.getMileage() > 0 && alarm.getMileage() < currentMileage) {
                mileage.setError(getString(R.string.error_mileage_too_low));
                valid = false;
            }
        }

        if (!alarm.isOnMileage() && !alarm.isOnDate()) {
            valid = false;
            Toast.makeText(this, getString(R.string.error_select_reminder_type), Toast.LENGTH_LONG).show();
        }

        return valid;
    }

    private CompoundButton.OnCheckedChangeListener dateSwitchOnChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
            if (checked) {
                alarm.setOnDate(true);
                date.enable();
                dateWarn.enable();
            } else {
                alarm.setOnDate(false);
                date.disable();
                dateWarn.disable();
            }
        }
    };

    private CompoundButton.OnCheckedChangeListener mileageSwitchOnChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
            if (checked) {
                alarm.setOnMileage(true);
                mileage.enable();
                mileageWarn.enable();
            } else {
                alarm.setOnMileage(false);
                mileage.disable();
                mileageWarn.disable();
            }
        }
    };
}
