package com.sianadev.car_alerts.reminder.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.textfield.TextInputEditText;
import com.sianadev.car_alerts.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public class SingleNumberInputFormDialog extends DialogFragment {
    private Listener listener;
    private int id;
    private TextInputEditText input;
    private String title;
    private String hint;
    private int value;

    public static SingleNumberInputFormDialog instance(int id, String title, String text) {
        SingleNumberInputFormDialog dialog = new SingleNumberInputFormDialog();
        dialog.setId(id);
        dialog.setTitle(title);
        dialog.setHint(text);
        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getContext());

        View layout = getActivity().getLayoutInflater().inflate(R.layout.single_number_input_form, null);
        input = layout.findViewById(R.id.input);
        input.setHint(hint);

        if (value > 0) {
            input.setText(String.valueOf(value));
        }

        builder.setView(layout)
                .setTitle(title)
                .setPositiveButton(getString(R.string.action_save), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listener.onSubmitSingleNumberInputForm(id, Integer.parseInt(input.getText().toString()));
                    }
                })
                .setNegativeButton(getString(R.string.action_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (Listener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException("Listener must implement interface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    private void setId(int id) {
        this.id = id;
    }

    private void setHint(String text) {
        hint = text;
    }

    private void setTitle(String text) {
        title = text;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public interface Listener {
        void onSubmitSingleNumberInputForm(int id, int value);
    }
}
