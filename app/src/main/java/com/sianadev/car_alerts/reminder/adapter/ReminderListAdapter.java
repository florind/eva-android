package com.sianadev.car_alerts.reminder.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.sianadev.car_alerts.R;
import com.sianadev.car_alerts.reminder.entity.Reminder;
import com.sianadev.car_alerts.reminder.factory.ReminderFactory;
import com.sianadev.car_alerts.reminder.helper.AlarmEstimator;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ReminderListAdapter extends RecyclerView.Adapter<ReminderListAdapter.ViewHolder> {
    private List<Reminder> reminderList;
    private AlarmEstimator estimator;
    private Listener listener;

    public ReminderListAdapter(Listener listener, AlarmEstimator estimator) {
        reminderList = ReminderFactory.getDefaultReminders();
        sortReminders();
        this.estimator = estimator;
        this.listener = listener;
    }

    public void setReminderList(List<Reminder> list) {
        reminderList = ReminderFactory.getDefaultReminders();
        for (Reminder reminder : list) {
            if (reminder.getType() != Reminder.TYPE_CUSTOM) {
                for (int i = 0; i < reminderList.size(); i++) {
                    if (reminder.getType() == reminderList.get(i).getType()) {
                        reminderList.set(i, reminder);
                    }
                }
            } else {
                reminderList.add(reminder);
            }
        }
        sortReminders();
        notifyDataSetChanged();
    }

    private void sortReminders() {
        Collections.sort(reminderList, new Comparator<Reminder>() {
            @Override
            public int compare(Reminder r1, Reminder r2) {
                if (r1.hasBeenSet() && !r2.hasBeenSet()) {
                    return -1;
                } else if (!r1.hasBeenSet() && r2.hasBeenSet()) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.reminder_card, parent, false);
        return new ReminderListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.alarm = reminderList.get(position);
        holder.title.setText(holder.alarm.getTitle());
        holder.eta.setText(estimator.estimate(holder.alarm));

        holder.title.setEnabled(holder.alarm.hasBeenSet());
        holder.eta.setEnabled(holder.alarm.hasBeenSet());
        holder.onIcon.setVisibility(holder.alarm.hasBeenSet() ? View.VISIBLE: View.GONE);
        holder.offIcon.setVisibility(!holder.alarm.hasBeenSet() ? View.VISIBLE: View.GONE);
    }

    @Override
    public int getItemCount() {
        return reminderList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View view;
        public final View layout;
        public final TextView title;
        public final TextView eta;
        public final ImageView onIcon;
        public final ImageView offIcon;
        public final ImageButton options;
        public Reminder alarm;

        ViewHolder(View view) {
            super(view);
            this.view = view;
            layout = view.findViewById(R.id.layout);
            title = view.findViewById(R.id.title);
            eta = view.findViewById(R.id.eta);
            onIcon = view.findViewById(R.id.on_icon);
            offIcon = view.findViewById(R.id.off_icon);
            options = view.findViewById(R.id.options);

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onAlarmClick(alarm);
                }
            });

            options.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onAlarmOptionMenu(view, alarm);
                }
            });
        }
    }

    public interface Listener {
        void onAlarmClick(Reminder alarm);
        void onAlarmOptionMenu(View view, Reminder alarm);
    }
}
