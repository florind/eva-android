package com.sianadev.car_alerts.reminder.work;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.sianadev.car_alerts.R;
import com.sianadev.car_alerts.reminder.NotificationActivity;
import com.sianadev.car_alerts.reminder.entity.AlarmNotification;
import com.sianadev.car_alerts.reminder.receiver.PostponeMileageReadBroadcastReceiver;
import com.sianadev.car_alerts.reminder.service.NotificationService;
import com.sianadev.car_alerts.main.MainActivity;
import com.sianadev.car_alerts.main.service.DatabaseService;
import com.sianadev.car_alerts.vehicle.entity.Vehicle;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.app.TaskStackBuilder;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

public class NotificationsWorker extends Worker {
    private NotificationService notificationService;

    public NotificationsWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        notificationService = new NotificationService(context);
    }

    @NonNull
    @Override
    public Result doWork() {
        notifyUpcomingEvents();
        notifyMileageRead();

        return Result.SUCCESS;
    }

    private void notifyUpcomingEvents() {
        DatabaseService database = DatabaseService.instance(getApplicationContext());
        List<AlarmNotification> notifications = database.alarmDao().getNotifications();
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());

        for (AlarmNotification notification : notifications) {
            Intent intent = new Intent(getApplicationContext(), NotificationActivity.class);
            intent.putExtra("ALARM_ID", notification.getAlarmId());
            stackBuilder.addNextIntentWithParentStack(intent);
            PendingIntent pendingIntent = stackBuilder.getPendingIntent(notification.getAlarmId(), PendingIntent.FLAG_UPDATE_CURRENT);
            notificationService.build(notification.getTitle(), notification.getVehicleModel(), notification.getDescription(), pendingIntent, null);
        }
    }

    private void notifyMileageRead() {
        DatabaseService database = DatabaseService.instance(getApplicationContext());
        List<Vehicle> vehicles = database.vehicleDao().getPendingMileageRead();
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());

        for (Vehicle vehicle : vehicles) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.putExtra("ACTION", "MILEAGE_READ");
            intent.putExtra("VEHICLE_ID", vehicle.getId());
            stackBuilder.addNextIntentWithParentStack(intent);
            PendingIntent pendingIntent = stackBuilder.getPendingIntent((int) System.currentTimeMillis(), PendingIntent.FLAG_CANCEL_CURRENT);

            Intent postponeIntent = new Intent(getApplicationContext(), PostponeMileageReadBroadcastReceiver.class);
            postponeIntent.setAction(PostponeMileageReadBroadcastReceiver.ACTION);
            postponeIntent.putExtra("VEHICLE_ID", vehicle.getId());
            PendingIntent postponePendingIntent = PendingIntent.getBroadcast(getApplicationContext(), (int)System.currentTimeMillis(), postponeIntent, PendingIntent.FLAG_CANCEL_CURRENT);

            notificationService.build(
                    vehicle.getModel(),
                    getApplicationContext().getString(R.string.mileage_read_notification_title),
                    getApplicationContext().getString(R.string.mileage_read_notification_description),
                    pendingIntent,
                    postponePendingIntent
            );
        }
    }
}
