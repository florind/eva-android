package com.sianadev.car_alerts.reminder;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;

import com.sianadev.car_alerts.R;
import com.sianadev.car_alerts.reminder.entity.Reminder;
import com.sianadev.car_alerts.reminder.fragment.SingleTextInputFormDialog;
import com.sianadev.car_alerts.reminder.helper.TimeIntervalFactory;
import com.sianadev.car_alerts.reminder.model.ReminderViewModel;
import com.sianadev.car_alerts.reminder.repository.ReminderRepository;
import com.sianadev.car_alerts.main.view.SettingView;
import com.sianadev.car_alerts.main.view.SwitchSettingView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

public class CreateAlarmActivity extends AppCompatActivity implements SingleTextInputFormDialog.Listener{
    private static final int ALARM_SETTINGS_REQUEST = 990;
    private static final int ALARM_REPEAT_CHECK_REQUEST = 37;
    private static final int ALARM_REPEAT_SETTINGS_REQUEST = 905;
    private static final int TITLE_INPUT_ID = 356;
    private static final int DESCRIPTION_INPUT_ID = 46;

    private Reminder alarm;
    private int currentMileage;
    private SettingView titleView, descriptionView, alarmSettingsView, repeatAlarmSettingsView;
    private SwitchSettingView repeatAlarmSwitch;
    private TimeIntervalFactory timeIntervalFactory;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_alarm);

        alarm = new Reminder();
        titleView = findViewById(R.id.alarm_title);
        titleView.setTitle(getString(R.string.reminder_title));
        titleView.setDescription(getString(R.string.not_set));
        titleView.setListener(titleOnClickListener);

        descriptionView = findViewById(R.id.alarm_description);
        descriptionView.setTitle(getString(R.string.reminder_description));
        descriptionView.setDescription(getString(R.string.not_set));
        descriptionView.setListener(descriptionOnClickListener);

        timeIntervalFactory = TimeIntervalFactory.instance();
        handleIntent();

        alarmSettingsView = findViewById(R.id.alarm_settings);
        alarmSettingsView.setTitle(getString(R.string.reminder_settings_title));
        alarmSettingsView.setDescription(getString(R.string.not_set));
        alarmSettingsView.setListener(onAlarmSettingsViewClickListener);

        repeatAlarmSwitch = findViewById(R.id.repeat_alarm);
        repeatAlarmSwitch.setTitle(getString(R.string.reminder_repeat_switch_title));
        repeatAlarmSwitch.setDescription(getString(R.string.reminder_repeat_switch_disabled_description));
        repeatAlarmSwitch.setListener(repeatSwitchListener);
        repeatAlarmSwitch.setEnabled(false);

        repeatAlarmSettingsView = findViewById(R.id.repeat_alarm_settings);
        repeatAlarmSettingsView.setTitle(getString(R.string.reminder_repeat_interval_title));
        repeatAlarmSettingsView.setDescription(getString(R.string.not_set));
        repeatAlarmSettingsView.setListener(repeatSettingsListener);
    }

    private void handleIntent() {
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            int alarmId = extras.getInt("REMINDER_ID", 0);
            alarm.setVehicleId(extras.getInt("VEHICLE_ID"));
            currentMileage = extras.getInt("MILEAGE");

            if (alarmId > 0) {
                ReminderViewModel alarmViewModel = ViewModelProviders.of(this).get(ReminderViewModel.class);
                alarmViewModel.get(alarmId).observe(this, alarmObserver);
            }
        } else {
            finish();
        }
    }

    private void refreshAlarmSettingsView() {
        if (alarm.isOnDate() && alarm.isOnMileage()) {
            String date = alarm.getDate(getString(R.string.date_format));
            String mileage = getString(R.string.mileage_read, String.valueOf(alarm.getMileage()));
            alarmSettingsView.setDescription(getString(R.string.reminder_date_mileage_eta, date, mileage));
        } else if (alarm.isOnDate()) {
            String date = alarm.getDate(getString(R.string.date_format));
            alarmSettingsView.setDescription(date);
        } else if (alarm.isOnMileage()) {
            String mileage = getString(R.string.mileage_read, String.valueOf(alarm.getMileage()));
            alarmSettingsView.setDescription(mileage);
        }
    }

    private void refreshRepeatAlarmSettings() {
        if (alarm.isOnDate() && alarm.isOnMileage()) {
            String time = timeIntervalFactory.from(alarm.getRepeatTimeInterval()).toString();
            String mileage = getString(R.string.mileage_read, String.valueOf(alarm.getRepeatMileage()));
            repeatAlarmSettingsView.setDescription(getString(R.string.reminder_date_mileage_eta, time, mileage));
        } else if (alarm.isOnDate()) {
            String time = timeIntervalFactory.from(alarm.getRepeatTimeInterval()).toString();
            repeatAlarmSettingsView.setDescription(time);
        } else if (alarm.isOnMileage()) {
            String mileage = getString(R.string.mileage_read, String.valueOf(alarm.getRepeatMileage()));
            repeatAlarmSettingsView.setDescription(mileage);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        if (item.getItemId() == R.id.action_save) {
            createAlarm();
        }
        return super.onOptionsItemSelected(item);
    }

    private void createAlarm() {
        if (!alarmIsValid()) {
            return;
        }

        alarm.setActive(true);
        ReminderRepository repository = new ReminderRepository(getApplication());
        repository.save(alarm);

        finish();
    }

    private boolean alarmIsValid() {
        boolean valid = true;

        if (!alarm.isOnDate() && !alarm.isOnMileage()) {
            alarmSettingsView.setError(getString(R.string.not_set_error));
            valid = false;
        }

        if (alarm.getTitle() == null || alarm.getTitle().trim().isEmpty()) {
            titleView.setError(getString(R.string.not_set_error));
            valid = false;
        }

        return valid;
    }

    private View.OnClickListener onAlarmSettingsViewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent alarmSettingsIntent = new Intent(CreateAlarmActivity.this, AlarmSettingsActivity.class);

            Bundle bundle = new Bundle();
            bundle.putSerializable(Reminder.class.toString(), alarm);
            bundle.putInt("MILEAGE", currentMileage);

            alarmSettingsIntent.putExtras(bundle);
            startActivityForResult(alarmSettingsIntent, ALARM_SETTINGS_REQUEST);
        }
    };

    private CompoundButton.OnCheckedChangeListener repeatSwitchListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
            if (alarm.isRepeating() && checked) {
                repeatAlarmSettingsView.setVisibility(View.VISIBLE);
                return;
            }

            if (checked) {
                startRepeatAlarmSettingsActivity(ALARM_REPEAT_CHECK_REQUEST);
            } else {
                repeatAlarmSettingsView.setVisibility(View.GONE);
            }
        }
    };

    private View.OnClickListener repeatSettingsListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startRepeatAlarmSettingsActivity(ALARM_REPEAT_SETTINGS_REQUEST);
        }
    };

    private View.OnClickListener titleOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            SingleTextInputFormDialog dialog = SingleTextInputFormDialog.instance(
                    TITLE_INPUT_ID,
                    getString(R.string.reminder_dialog_title),
                    ""
            );
            dialog.setValue(alarm.getTitle());
            dialog.show(getSupportFragmentManager(), dialog.getClass().toString());
        }
    };

    private View.OnClickListener descriptionOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            SingleTextInputFormDialog dialog = SingleTextInputFormDialog.instance(
                    DESCRIPTION_INPUT_ID,
                    getString(R.string.reminder_dialog_description),
                    ""
            );
            dialog.setValue(alarm.getDescription());
            dialog.setMultiline(true);
            dialog.show(getSupportFragmentManager(), dialog.getClass().toString());
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == ALARM_SETTINGS_REQUEST && resultCode == Activity.RESULT_OK) {
            if (data != null && data.getExtras() != null) {
                alarm = (Reminder) data.getSerializableExtra(Reminder.class.toString());
                repeatAlarmSwitch.clearDescription();
                repeatAlarmSwitch.setEnabled(true);
                refreshAlarmSettingsView();
            }
        }

        if (requestCode == ALARM_REPEAT_CHECK_REQUEST) {
            if (resultCode == Activity.RESULT_OK && data != null && data.getExtras() != null) {
                alarm = (Reminder) data.getSerializableExtra(Reminder.class.toString());
                repeatAlarmSettingsView.setVisibility(View.VISIBLE);
                refreshRepeatAlarmSettings();
            } else {
                repeatAlarmSwitch.uncheck();
            }
        }

        if (requestCode == ALARM_REPEAT_SETTINGS_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getExtras() != null) {
            alarm = (Reminder) data.getSerializableExtra(Reminder.class.toString());
            refreshRepeatAlarmSettings();
        }
    }

    private Observer<Reminder> alarmObserver = new Observer<Reminder>() {
        @Override
        public void onChanged(@Nullable Reminder foundAlarm) {
            if (foundAlarm != null) {
                alarm = foundAlarm;
                titleView.setDescription(alarm.getTitle());
                descriptionView.setDescription(alarm.getDescription());

                if (alarm.isOnDate() || alarm.isOnMileage()) {
                    repeatAlarmSwitch.clearDescription();
                    repeatAlarmSwitch.setEnabled(true);
                }

                if (alarm.isRepeating()) {
                    repeatAlarmSwitch.check();
                }

                refreshAlarmSettingsView();
                refreshRepeatAlarmSettings();
            }
        }
    };

    @Override
    public void onSubmitSingleTextInputForm(int id, String value) {
        if (id == TITLE_INPUT_ID) {
            alarm.setTitle(value);
            titleView.setDescription(value);
        }

        if (id == DESCRIPTION_INPUT_ID) {
            alarm.setDescription(value);
            descriptionView.setDescription(value);
        }
    }

    private void startRepeatAlarmSettingsActivity(int requestCode) {
        Intent alarmSettingsIntent = new Intent(CreateAlarmActivity.this, RepeatAlarmActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Reminder.class.toString(), alarm);
        alarmSettingsIntent.putExtras(bundle);
        startActivityForResult(alarmSettingsIntent, requestCode);
    }
}
