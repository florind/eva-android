package com.sianadev.car_alerts.reminder.factory;

import com.sianadev.car_alerts.R;
import com.sianadev.car_alerts.reminder.entity.Reminder;
import com.sianadev.car_alerts.main.App;

import java.util.ArrayList;

public class ReminderFactory {
    public static ArrayList<Reminder> getDefaultReminders() {
        ArrayList<Reminder> defaultReminders = new ArrayList<>();

        Reminder timingBelt = new Reminder();
        timingBelt.setType(Reminder.TYPE_TIMING_BELT);
        timingBelt.setTitle(App.resources().getString(R.string.timing_belt_reminder_default_title));

        Reminder oilChange = new Reminder();
        oilChange.setType(Reminder.TYPE_OIL_CHANGE);
        oilChange.setTitle(App.resources().getString(R.string.oil_change_reminder_default_title));

        Reminder sparkPlugs = new Reminder();
        sparkPlugs.setType(Reminder.TYPE_SPARK_PLUGS);
        sparkPlugs.setTitle(App.resources().getString(R.string.spark_plugs_reminder_default_title));

        Reminder antifreeze = new Reminder();
        antifreeze.setType(Reminder.TYPE_ANTIFREEZE);
        antifreeze.setTitle(App.resources().getString(R.string.antifreeze_reminder_default_title));

        Reminder brakeFluid = new Reminder();
        brakeFluid.setType(Reminder.TYPE_BRAKE_FLUID);
        brakeFluid.setTitle(App.resources().getString(R.string.brake_fluid_reminder_default_title));

        Reminder insurance = new Reminder();
        insurance.setType(Reminder.TYPE_INSURANCE);
        insurance.setTitle(App.resources().getString(R.string.insurance_reminder_default_title));

        Reminder technicalInspection = new Reminder();
        technicalInspection.setType(Reminder.TYPE_TECHNICAL_INSPECTION);
        technicalInspection.setTitle(App.resources().getString(R.string.technical_inspection_reminder_default_title));

        Reminder vignette = new Reminder();
        vignette.setType(Reminder.TYPE_VIGNETTE);
        vignette.setTitle(App.resources().getString(R.string.vignette_reminder_default_title));

        Reminder medicalKit = new Reminder();
        medicalKit.setType(Reminder.TYPE_MEDICAL_KIT);
        medicalKit.setTitle(App.resources().getString(R.string.medical_kit_reminder_default_title));

        Reminder extinguisher = new Reminder();
        extinguisher.setType(Reminder.TYPE_EXTINGUISHER);
        extinguisher.setTitle(App.resources().getString(R.string.extinguisher_reminder_default_title));

        defaultReminders.add(timingBelt);
        defaultReminders.add(oilChange);
        defaultReminders.add(sparkPlugs);
        defaultReminders.add(antifreeze);
        defaultReminders.add(brakeFluid);
        defaultReminders.add(insurance);
        defaultReminders.add(vignette);
        defaultReminders.add(technicalInspection);
        defaultReminders.add(medicalKit);
        defaultReminders.add(extinguisher);

        return defaultReminders;
    }
}
