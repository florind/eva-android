package com.sianadev.car_alerts.reminder.entity;

import java.util.Date;

public interface EstimableAlarmInterface {
    boolean isOnDate();
    boolean isOnMileage();
    Date getDate();
    int getMileage();
}
