package com.sianadev.car_alerts.reminder.entity;

import java.util.Date;

public class AlarmNotification implements EstimableAlarmInterface {
    private int alarmId;
    private String title;
    private String description;
    private boolean onDate;
    private boolean onMileage;
    private Date date;
    private int mileage;
    private boolean active;
    private String vehicleModel;
    private int vehicleMileage;

    public int getAlarmId() {
        return alarmId;
    }

    public void setAlarmId(int alarmId) {
        this.alarmId = alarmId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean isOnDate() {
        return onDate;
    }

    public void setOnDate(boolean onDate) {
        this.onDate = onDate;
    }

    @Override
    public boolean isOnMileage() {
        return onMileage;
    }

    public void setOnMileage(boolean onMileage) {
        this.onMileage = onMileage;
    }

    @Override
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public int getVehicleMileage() {
        return vehicleMileage;
    }

    public void setVehicleMileage(int vehicleMileage) {
        this.vehicleMileage = vehicleMileage;
    }
}
