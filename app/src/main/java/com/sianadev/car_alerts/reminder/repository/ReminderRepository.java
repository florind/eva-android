package com.sianadev.car_alerts.reminder.repository;

import android.content.Context;
import android.os.AsyncTask;

import com.sianadev.car_alerts.reminder.dao.ReminderDao;
import com.sianadev.car_alerts.reminder.entity.Reminder;
import com.sianadev.car_alerts.reminder.entity.AlarmNotification;
import com.sianadev.car_alerts.main.service.DatabaseService;

import java.util.List;

import androidx.lifecycle.LiveData;

public class ReminderRepository {
    private ReminderDao dao;

    public ReminderRepository(Context context) {
        DatabaseService db = DatabaseService.instance(context);
        dao = db.alarmDao();
    }

    public LiveData<List<Reminder>> list(int vehicleId) {
        return dao.list(vehicleId);
    }

    public LiveData<Reminder> get(int id) {
        return dao.get(id);
    }

    public void save(Reminder alarm) {
        new saveAsyncTask(dao).execute(alarm);
    }

    public void flush(int vehicleId) {
        new flushAsyncTask(dao).execute(vehicleId);
    }

    public void delete(Reminder alarm) {
        new deleteAsyncTask(dao).execute(alarm);
    }

    public LiveData<List<AlarmNotification>> getNotificationsLiveData() {
        return dao.getNotificationsLiveData();
    }

    private static class saveAsyncTask extends AsyncTask<Reminder, Void, Void> {
        private ReminderDao dao;

        saveAsyncTask(ReminderDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(Reminder... alarms) {
            dao.save(alarms[0]);
            return null;
        }
    }

    private static class deleteAsyncTask extends AsyncTask<Reminder, Void, Void> {
        private ReminderDao dao;

        deleteAsyncTask(ReminderDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(Reminder... alarms) {
            dao.delete(alarms[0]);
            return null;
        }
    }

    private static class flushAsyncTask extends AsyncTask<Integer, Void, Void> {
        private ReminderDao dao;

        flushAsyncTask(ReminderDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(Integer... params) {
            dao.flush(params[0]);
            return null;
        }
    }
}
