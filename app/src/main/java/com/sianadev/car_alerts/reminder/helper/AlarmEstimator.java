package com.sianadev.car_alerts.reminder.helper;

import android.content.Context;

import com.sianadev.car_alerts.R;
import com.sianadev.car_alerts.reminder.entity.EstimableAlarmInterface;

import java.util.Calendar;

public class AlarmEstimator {
    private int mileage;
    private Context context;

    public AlarmEstimator(Context context) {
        this.context = context;
    }

    public void setCurrentVehicleMileage(int mileage) {
        this.mileage = mileage;
    }

    public String estimate(EstimableAlarmInterface alarm) {
        if (alarm.isOnDate() && alarm.isOnMileage()) {
            return estimateTimeAndMileage(alarm);
        } else if (alarm.isOnDate()) {
            return estimateTime(alarm);
        } else if (alarm.isOnMileage()) {
            return estimateMileage(alarm);
        }

        return context.getResources().getString(R.string.not_set);
    }

    private String estimateTime(EstimableAlarmInterface alarm) {
        long today = Calendar.getInstance().getTime().getTime();
        long date = alarm.getDate().getTime();
        long diff = date - today;
        // +1 to take the day of the alarm into account
        long days = diff / 1000 / 60 / 60 / 24 + 1;

        if (days < 0) {
            return context.getString(R.string.reminder_date_overdue, String.valueOf(Math.abs(days)));
        }

        return context.getString(R.string.reminder_date_eta, String.valueOf(days));
    }

    private String estimateMileage(EstimableAlarmInterface alarm) {
        int milesToGo = alarm.getMileage() - mileage;

        if (milesToGo < 0) {
            return context.getString(R.string.reminder_mileage_overdue, String.valueOf(Math.abs(milesToGo)));
        }

        return context.getString(R.string.reminder_mileage_eta, String.valueOf(milesToGo));
    }

    private String estimateTimeAndMileage(EstimableAlarmInterface alarm) {
        return context.getString(R.string.reminder_date_mileage_eta, estimateMileage(alarm), estimateTime(alarm));
    }
}
