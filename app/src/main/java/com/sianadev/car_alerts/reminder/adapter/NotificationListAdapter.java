package com.sianadev.car_alerts.reminder.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sianadev.car_alerts.R;
import com.sianadev.car_alerts.reminder.entity.AlarmNotification;
import com.sianadev.car_alerts.reminder.helper.AlarmEstimator;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.ViewHolder> {
    private List<AlarmNotification> notifications;
    private Listener listener;
    private Context context;

    public NotificationListAdapter(Listener listener, Context context) {
        notifications = new ArrayList<>();
        this.context = context;
        this.listener = listener;
    }

    public void setNotifications(List<AlarmNotification> notifications) {
        this.notifications = notifications;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.upcoming_alarm_card, parent, false);
        return new NotificationListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.upcomingAlarm = notifications.get(position);
        holder.entityTitle.setText(holder.upcomingAlarm.getVehicleModel());
        holder.alarmTitle.setText(holder.upcomingAlarm.getTitle());

        AlarmEstimator estimator = new AlarmEstimator(context);
        estimator.setCurrentVehicleMileage(holder.upcomingAlarm.getVehicleMileage());
        holder.eta.setText(estimator.estimate(holder.upcomingAlarm));
    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View view;
        public final TextView entityTitle;
        public final TextView alarmTitle;
        public final TextView eta;
        public AlarmNotification upcomingAlarm;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            entityTitle = view.findViewById(R.id.entity_title);
            alarmTitle = view.findViewById(R.id.alarm_title);
            eta = view.findViewById(R.id.eta);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onAlarmOptionsMenuClick(view, upcomingAlarm);
                }
            });
        }
    }

    public interface Listener {
        void onAlarmOptionsMenuClick(View view, AlarmNotification alarm);
    }
}
