package com.sianadev.car_alerts.reminder.helper;

import java.util.Calendar;
import java.util.Date;

public class TimeOperations {
    private static final int POSITIVE = 1;
    private static final int NEGATIVE = -1;

    public static Date add(Date date, String interval) {
        return calculate(date, interval, POSITIVE);
    }

    public static Date substract(Date date, String interval) {
        return calculate(date, interval, NEGATIVE);
    }

    private static Date calculate(Date date, String interval, int operator) {
        String[] parts = interval.split(" ");

        if (parts.length != 2) {
            return date;
        }

        int value = Integer.parseInt(parts[0]);
        String unit = parts[1];

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 1);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        switch (unit) {
            case "d":
                calendar.add(Calendar.DATE, value * operator);
                break;
            case "m":
                calendar.add(Calendar.MONTH, value * operator);
                break;
            case "y":
                calendar.add(Calendar.YEAR, value * operator);
                break;
        }

        return calendar.getTime();
    }

}
