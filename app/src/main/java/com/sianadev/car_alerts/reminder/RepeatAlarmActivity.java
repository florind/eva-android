package com.sianadev.car_alerts.reminder;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.sianadev.car_alerts.R;
import com.sianadev.car_alerts.reminder.entity.Reminder;
import com.sianadev.car_alerts.reminder.fragment.SingleNumberInputFormDialog;
import com.sianadev.car_alerts.reminder.fragment.TimeIntervalPickerDialog;
import com.sianadev.car_alerts.reminder.helper.TimeIntervalFactory;
import com.sianadev.car_alerts.main.view.SettingView;

import androidx.appcompat.app.AppCompatActivity;

public class RepeatAlarmActivity extends AppCompatActivity implements SingleNumberInputFormDialog.Listener, TimeIntervalPickerDialog.Listener {
    private static final int MILEAGE_INPUT_ID = 251;

    Reminder alarm;
    SettingView timeSetting, mileageSetting;
    TimeIntervalFactory timeIntervalFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repeat_alarm);

        timeIntervalFactory = TimeIntervalFactory.instance();

        timeSetting = findViewById(R.id.time);
        timeSetting.setTitle(getString(R.string.repeat_reminder_date_title));
        timeSetting.setDescription(getString(R.string.not_set));
        timeSetting.setListener(timeOnClickListener);

        mileageSetting = findViewById(R.id.mileage);
        mileageSetting.setTitle(getString(R.string.repeat_reminder_mileage_title));
        mileageSetting.setDescription(getString(R.string.not_set));
        mileageSetting.setListener(mileageOnClickListener);

        handleIntent();
    }

    private void handleIntent() {
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            alarm = (Reminder) bundle.getSerializable(Reminder.class.toString());

            if (alarm != null) {
                if (alarm.isOnDate()) {
                    timeSetting.setVisibility(View.VISIBLE);
                    timeSetting.setDescription(timeIntervalFactory.from(alarm.getRepeatTimeInterval()).toString());
                }

                if (alarm.isOnMileage()) {
                    mileageSetting.setVisibility(View.VISIBLE);
                    mileageSetting.setDescription(getString(R.string.mileage_read, String.valueOf(alarm.getRepeatMileage())));
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.ok_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent response = new Intent();
            setResult(Activity.RESULT_CANCELED, response);
            finish();
            return true;
        }

        if (item.getItemId() == R.id.action_ok) {
            if (settingsAreValid()) {
                alarm.setRepeating(true);
                Intent response = new Intent();
                response.putExtra(Reminder.class.toString(), alarm);
                setResult(Activity.RESULT_OK, response);
                finish();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean settingsAreValid() {
        boolean valid = true;

        if (alarm.isOnDate() && alarm.getRepeatTimeInterval().trim().isEmpty()) {
            timeSetting.setError(getString(R.string.not_set_error));
            valid = false;
        }

        if (alarm.isOnMileage() && alarm.getRepeatMileage() == 0) {
            mileageSetting.setError(getString(R.string.not_set_error));
            valid = false;
        }

        return valid;
    }

    private View.OnClickListener timeOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            TimeIntervalPickerDialog dialog = TimeIntervalPickerDialog.instance(getString(R.string.reminder_dialog_time_repeat_title), 0);
            dialog.show(getSupportFragmentManager(), dialog.getClass().toString());
        }
    };

    private View.OnClickListener mileageOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            SingleNumberInputFormDialog dialog = SingleNumberInputFormDialog.instance(
                    MILEAGE_INPUT_ID,
                    getString(R.string.reminder_dialog_mileage_repeat_title),
                    String.valueOf(alarm.getRepeatMileage())
            );
            dialog.setValue(alarm.getRepeatMileage());
            dialog.show(getSupportFragmentManager(), dialog.getClass().toString());
        }
    };

    @Override
    public void onSubmitSingleNumberInputForm(int id, int value) {
        if (id == MILEAGE_INPUT_ID) {
            alarm.setRepeatMileage(value);
            mileageSetting.setDescription(getString(R.string.mileage_read, String.valueOf(alarm.getRepeatMileage())));
        }
    }

    @Override
    public void onTimeIntervalSelected(int requestCode, TimeIntervalFactory.TimeInterval interval) {
        alarm.setRepeatTimeInterval(interval.getValue());
        timeSetting.setDescription(interval.toString());
    }
}
