package com.sianadev.car_alerts.reminder.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.sianadev.car_alerts.R;
import com.sianadev.car_alerts.reminder.adapter.ReminderListAdapter;
import com.sianadev.car_alerts.reminder.entity.Reminder;
import com.sianadev.car_alerts.reminder.helper.AlarmEstimator;
import com.sianadev.car_alerts.reminder.model.ReminderViewModel;
import com.sianadev.car_alerts.reminder.repository.ReminderRepository;
import com.sianadev.car_alerts.reminder.type.CustomReminder;
import com.sianadev.car_alerts.reminder.type.DateReminder;
import com.sianadev.car_alerts.reminder.type.MileageDateReminder;
import com.sianadev.car_alerts.reminder.type.RepeatableDateReminder;
import com.sianadev.car_alerts.vehicle.entity.Vehicle;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ReminderListFragment extends Fragment implements ReminderListAdapter.Listener {

    private Vehicle vehicle;
    private TextView reminderListTitle;
    private ReminderListAdapter adapter;
    private ReminderRepository repository;

    @Override
    public void setArguments(@Nullable Bundle args) {
        super.setArguments(args);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        AlarmEstimator alarmEstimator = new AlarmEstimator(getContext());
        repository = new ReminderRepository(getContext());

        if (args != null) {
            vehicle = (Vehicle) args.getSerializable("VEHICLE");

            if (vehicle == null) {
                getActivity().getSupportFragmentManager().popBackStack();
            }

            alarmEstimator.setCurrentVehicleMileage(vehicle.getMileage());
        }

        adapter = new ReminderListAdapter(this, alarmEstimator);
        ReminderViewModel alarmList = ViewModelProviders.of(getActivity()).get(ReminderViewModel.class);

        alarmList.list(vehicle.getId()).observe(this, new Observer<List<Reminder>>() {
            @Override
            public void onChanged(@Nullable List<Reminder> alarms) {
                adapter.setReminderList(alarms);
                reminderListTitle.setText(getString(R.string.reminder_list_title));
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.reminder_list, container, false);

        reminderListTitle = view.findViewById(R.id.reminder_list_title);
        FloatingActionButton addAlarm = view.findViewById(R.id.add_alarm);

        addAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), CustomReminder.class);

                Reminder customReminder = new Reminder();
                customReminder.setType(Reminder.TYPE_CUSTOM);
                customReminder.setTitle(getString(R.string.custom_reminder_activity_title));

                Bundle bundle = new Bundle();
                bundle.putSerializable("REMINDER", customReminder);
                bundle.putSerializable("VEHICLE", vehicle);

                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        // Set the adapter
        if (view.findViewById(R.id.list) instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = view.findViewById(R.id.list);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(adapter);
        }

        return view;
    }

    @Override
    public void onAlarmOptionMenu(View view, final Reminder alarm) {
        PopupMenu menu = new PopupMenu(getContext(), view);
        menu.getMenuInflater().inflate(R.menu.alarm_context_menu, menu.getMenu());
        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.delete:
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setMessage(getString(R.string.confirm_delete_message, alarm.getTitle()))
                                .setPositiveButton(getString(R.string.action_delete), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        repository.delete(alarm);
                                    }
                                })
                                .setNegativeButton(getString(R.string.action_cancel), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                }).create().show();
                        break;
                }

                return true;
            }
        });
        menu.show();
    }

    @Override
    public void onAlarmClick(Reminder reminder) {
        Class targetActivity;

        if (reminder.getType() == Reminder.TYPE_TIMING_BELT || reminder.getType() == Reminder.TYPE_OIL_CHANGE || reminder.getType() == Reminder.TYPE_SPARK_PLUGS || reminder.getType() == Reminder.TYPE_ANTIFREEZE || reminder.getType() == Reminder.TYPE_BRAKE_FLUID) {
            targetActivity = MileageDateReminder.class;
        } else if (reminder.getType() == Reminder.TYPE_INSURANCE || reminder.getType() == Reminder.TYPE_TECHNICAL_INSPECTION || reminder.getType() == Reminder.TYPE_VIGNETTE) {
            targetActivity = RepeatableDateReminder.class;
        } else if (reminder.getType() == Reminder.TYPE_MEDICAL_KIT || reminder.getType() == Reminder.TYPE_EXTINGUISHER) {
            targetActivity = DateReminder.class;
        } else {
            targetActivity = CustomReminder.class;
        }

        Intent intent = new Intent(getContext(), targetActivity);
        Bundle bundle = new Bundle();
        bundle.putSerializable("REMINDER", reminder);
        bundle.putSerializable("VEHICLE", vehicle);

        intent.putExtras(bundle);
        startActivity(intent);
    }
}
