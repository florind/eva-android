package com.sianadev.car_alerts.reminder.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.NumberPicker;

import com.sianadev.car_alerts.R;
import com.sianadev.car_alerts.reminder.helper.TimeIntervalFactory;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public class TimeIntervalPickerDialog extends DialogFragment {
    private Listener listener;
    private String title;
    private int requestCode;

    public static TimeIntervalPickerDialog instance(String text, int requestCode) {
        TimeIntervalPickerDialog dialog = new TimeIntervalPickerDialog();
        dialog.setTitle(text);
        dialog.setRequestCode(requestCode);

        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        View layout = getActivity().getLayoutInflater().inflate(R.layout.time_interval_picker, null);

        final TimeIntervalFactory timeIntervalFactory = TimeIntervalFactory.instance();
        final NumberPicker valuePicker = layout.findViewById(R.id.value);
        valuePicker.setMinValue(1);
        valuePicker.setMaxValue(99);

        final NumberPicker typePicker = layout.findViewById(R.id.type);
        typePicker.setMinValue(0);
        typePicker.setMaxValue(2);
        typePicker.setDisplayedValues(timeIntervalFactory.getSingular());

        valuePicker.setOnScrollListener(new NumberPicker.OnScrollListener() {
            @Override
            public void onScrollStateChange(NumberPicker numberPicker, int i) {
                if (numberPicker.getValue() > 1) {
                    typePicker.setDisplayedValues(timeIntervalFactory.getPlural());
                } else {
                    typePicker.setDisplayedValues(timeIntervalFactory.getSingular());
                }
            }
        });

        builder.setView(layout)
                .setTitle(title)
                .setPositiveButton(getString(R.string.action_save), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        TimeIntervalFactory.TimeInterval interval = timeIntervalFactory.build(
                                valuePicker.getValue(),
                                typePicker.getValue()
                        );

                        listener.onTimeIntervalSelected(requestCode, interval);
                    }
                })
                .setNegativeButton(R.string.action_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        return builder.create();
    }

    public void setTitle(String text) {
        title = text;
    }

    public void setRequestCode(int value) {
        requestCode = value;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (Listener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException("Listener must implement interface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public interface Listener {
        void onTimeIntervalSelected(int requestCode, TimeIntervalFactory.TimeInterval interval);
    }
}
