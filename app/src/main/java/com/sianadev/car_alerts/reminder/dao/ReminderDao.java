package com.sianadev.car_alerts.reminder.dao;

import com.sianadev.car_alerts.reminder.entity.Reminder;
import com.sianadev.car_alerts.reminder.entity.AlarmNotification;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.RoomWarnings;

@Dao
public interface ReminderDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void save(Reminder alarm);

    @Query("SELECT * FROM reminders WHERE vehicleId = :vehicleId")
    LiveData<List<Reminder>> list(int vehicleId);

    @Query("SELECT * FROM reminders WHERE id=:id")
    LiveData<Reminder> get(int id);

    @Query("DELETE FROM reminders WHERE vehicleId = :vehicleId")
    void flush(int vehicleId);

    @Delete
    void delete(Reminder... alarms);

    @SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
    @Query("select reminders.id as alarmId, reminders.vehicleId, reminders.title, reminders.description, reminders.onDate, reminders.onMileage, reminders.date, reminders.mileage, reminders.earlyWarningDate, reminders.earlyWarningMileage, reminders.active, vehicles.id, vehicles.model as vehicleModel, vehicles.mileage as vehicleMileage " +
            "from reminders " +
            "left join vehicles on vehicles.id = reminders.vehicleId " +
            "where " +
            "reminders.active = 1 AND  " +
            "case " +
            "when reminders.onDate = 1 AND reminders.onMileage = 1 then strftime(\"%s\", \"now\") >= reminders.earlyWarningDate or reminders.mileage <= vehicles.mileage + reminders.earlyWarningMileage " +
            "when reminders.onDate = 1 then strftime(\"%s\", \"now\") >= reminders.earlyWarningDate " +
            "when reminders.onMileage = 1 then reminders.mileage <= vehicles.mileage + reminders.earlyWarningMileage " +
            "end")
    LiveData<List<AlarmNotification>> getNotificationsLiveData();

    @SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
    @Query("select reminders.id as alarmId, reminders.vehicleId, reminders.title, reminders.description, reminders.onDate, reminders.onMileage, reminders.date, reminders.mileage, reminders.earlyWarningDate, reminders.earlyWarningMileage, reminders.active, vehicles.id, vehicles.model as vehicleModel, vehicles.mileage as vehicleMileage " +
            "from reminders " +
            "left join vehicles on vehicles.id = reminders.vehicleId " +
            "where " +
            "reminders.active = 1 AND " +
            "case " +
            "when reminders.onDate = 1 AND reminders.onMileage = 1 then strftime(\"%s\", \"now\") >= reminders.earlyWarningDate or reminders.mileage <= vehicles.mileage + reminders.earlyWarningMileage " +
            "when reminders.onDate = 1 then strftime(\"%s\", \"now\") >= reminders.earlyWarningDate " +
            "when reminders.onMileage = 1 then reminders.mileage <= vehicles.mileage + reminders.earlyWarningMileage " +
            "end")
    List<AlarmNotification> getNotifications();
}
