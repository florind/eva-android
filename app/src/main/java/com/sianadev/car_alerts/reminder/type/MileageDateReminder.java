package com.sianadev.car_alerts.reminder.type;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Switch;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.sianadev.car_alerts.R;
import com.sianadev.car_alerts.reminder.config.ReminderLayoutConfig;
import com.sianadev.car_alerts.reminder.entity.Reminder;
import com.sianadev.car_alerts.reminder.fragment.TimeIntervalPickerDialog;
import com.sianadev.car_alerts.reminder.helper.TimeIntervalFactory;
import com.sianadev.car_alerts.reminder.repository.ReminderRepository;
import com.sianadev.car_alerts.vehicle.entity.Vehicle;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;

public class MileageDateReminder extends AppCompatActivity implements TimeIntervalPickerDialog.Listener {

    private static final int REPEAT_TIME_INTERVAL = 835;
    private static final int NOTIFICATION_TIME_INTERVAL = 470;

    private TextInputLayout titleLayout, detailsLayout,
            dateDeadlineLayout, mileageDeadlineLayout,
            repeatTimeIntervalLayout, repeatMileageIntervalLayout,
            notificationTimeLayout, notificationMileageLayout;

    private TextInputEditText titleInput, detailsInput,
            dateDeadlineInput, mileageDeadlineInput,
            repeatTimeIntervalInput, repeatMileageIntervalInput,
            notificationTimeInput, notificationMileageInput;

    private Calendar calendar;
    private Reminder reminder;
    private Vehicle vehicle;
    private ReminderRepository repository;
    private TimeIntervalFactory timeIntervalFactory;
    private ReminderLayoutConfig layoutConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialize();

        setTitle(reminder.getTitle());
        setContentView(layoutConfig.getLayout());

        calendar = Calendar.getInstance();
        repository = new ReminderRepository(getApplicationContext());
        timeIntervalFactory = TimeIntervalFactory.instance();

        titleLayout = findViewById(R.id.title_layout);
        detailsLayout = findViewById(R.id.details_layout);
        dateDeadlineLayout = findViewById(R.id.date_deadline_layout);
        mileageDeadlineLayout = findViewById(R.id.mileage_deadline_layout);
        repeatTimeIntervalLayout = findViewById(R.id.time_interval_layout);
        repeatMileageIntervalLayout = findViewById(R.id.mileage_interval_layout);
        notificationTimeLayout = findViewById(R.id.notification_time_layout);
        notificationMileageLayout = findViewById(R.id.notification_mileage_layout);

        titleInput = findViewById(R.id.title);
        detailsInput = findViewById(R.id.details);
        dateDeadlineInput = findViewById(R.id.date_deadline);
        mileageDeadlineInput = findViewById(R.id.mileage_deadline);
        repeatTimeIntervalInput = findViewById(R.id.time_interval);
        repeatMileageIntervalInput = findViewById(R.id.mileage_interval);
        notificationTimeInput = findViewById(R.id.notification_time);
        notificationMileageInput = findViewById(R.id.notification_mileage);

        dateDeadlineInput.setOnClickListener(datepickerListener);
        repeatTimeIntervalInput.setOnClickListener(repeatTimeIntervalListener);
        notificationTimeInput.setOnClickListener(notificationTimeIntervalListener);

        if (reminder.getId() == 0) {
            setupDefaultFieldValues();
        } else {
            populateFields();
        }
    }

    private void setupDefaultFieldValues() {
        titleInput.setText(reminder.getTitle());
        repeatTimeIntervalInput.setText(layoutConfig.getRepeatTimeInterval().toString());
        repeatMileageIntervalInput.setText(layoutConfig.getRepeatMileage());

        notificationTimeInput.setText(layoutConfig.getNotificationTimeInterval().toString());
        notificationMileageInput.setText(layoutConfig.getNotificationMileage());
    }

    private void initialize() {
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            reminder = (Reminder) extras.getSerializable("REMINDER");
            vehicle = (Vehicle) extras.getSerializable("VEHICLE");

            if (reminder == null || vehicle == null) { finish(); }

            reminder.setVehicleId(vehicle.getId());
            layoutConfig = ReminderLayoutConfig.instance(reminder.getType());

            if (reminder.getId() == 0) {
                reminder.setRepeatTimeInterval(layoutConfig.getRepeatTimeInterval().getValue());
                reminder.setEarlyWarningInterval(layoutConfig.getNotificationTimeInterval().getValue());
            }
        } else {
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent response = new Intent();
            setResult(Activity.RESULT_CANCELED, response);
            finish();
            return true;
        }

        if (item.getItemId() == R.id.action_save) {
            if (saveReminder()) {
                Intent response = new Intent();
                setResult(Activity.RESULT_OK, response);
                finish();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean saveReminder() {
        if (reminderIsValid()) {
            reminder.setTitle(titleInput.getText().toString().trim());
            reminder.setDescription(detailsInput.getText().toString().trim());
            reminder.setOnDate(true);
            reminder.setOnMileage(true);
            reminder.setDate(calendar.getTime());
            reminder.setMileage(Integer.parseInt(mileageDeadlineInput.getText().toString()));
            reminder.setRepeating(true);
            reminder.setRepeatMileage(Integer.parseInt(repeatMileageIntervalInput.getText().toString()));
            reminder.setEarlyWarningMileage(Integer.parseInt(notificationMileageInput.getText().toString()));

            reminder.setActive(true);
            repository.save(reminder);
            return true;
        }

        return false;
    }

    private boolean reminderIsValid() {
        boolean valid = true;

        if (titleInput.getText() == null || titleInput.getText().toString().trim().isEmpty()) {
            titleLayout.setError(getString(R.string.error_required));
            valid = false;
        }

        if (dateDeadlineInput.getText() == null || dateDeadlineInput.getText().toString().trim().isEmpty()) {
            dateDeadlineLayout.setError(getString(R.string.error_required));
            valid = false;
        }

        if (mileageDeadlineInput.getText() == null || mileageDeadlineInput.getText().toString().isEmpty()) {
            mileageDeadlineLayout.setError(getString(R.string.error_required));
            valid = false;
        } else if (Integer.parseInt(mileageDeadlineInput.getText().toString()) < vehicle.getMileage()) {
            mileageDeadlineLayout.setError(getString(R.string.error_mileage_too_low, String.valueOf(vehicle.getMileage())));
        }

        if (repeatTimeIntervalInput.getText() == null || repeatTimeIntervalInput.getText().toString().trim().isEmpty()) {
            repeatTimeIntervalLayout.setError(getString(R.string.error_required));
            valid = false;
        }

        if (repeatMileageIntervalInput.getText() == null || repeatMileageIntervalInput.getText().toString().isEmpty()) {
            repeatMileageIntervalLayout.setError(getString(R.string.error_required));
            valid = false;
        }

        if (notificationTimeInput.getText() == null || notificationTimeInput.getText().toString().trim().isEmpty()) {
            notificationTimeLayout.setError(getString(R.string.error_required));
            valid = false;
        }

        if (notificationMileageInput.getText() == null || notificationMileageInput.getText().toString().isEmpty()) {
            notificationMileageLayout.setError(getString(R.string.error_required));
            valid = false;
        }

        return valid;
    }

    private View.OnClickListener datepickerListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            DatePickerDialog dialog = new DatePickerDialog(
                    view.getContext(),
                    R.style.Datepicker,
                    datepickerChangedListener,
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)
            );
            dialog.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis());
            dialog.show();
        }
    };

    private DatePickerDialog.OnDateSetListener datepickerChangedListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, day);

            SimpleDateFormat dateFormatter = new SimpleDateFormat(getString(R.string.date_format), Locale.getDefault());
            dateDeadlineInput.setText(dateFormatter.format(calendar.getTime()));
        }
    };

    private View.OnClickListener repeatTimeIntervalListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            TimeIntervalPickerDialog dialog = TimeIntervalPickerDialog.instance(getString(R.string.reminder_dialog_time_repeat_title), REPEAT_TIME_INTERVAL);
            dialog.show(getSupportFragmentManager(), dialog.getClass().toString());
        }
    };

    private View.OnClickListener notificationTimeIntervalListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            TimeIntervalPickerDialog dialog = TimeIntervalPickerDialog.instance(getString(R.string.reminder_dialog_time_repeat_title), NOTIFICATION_TIME_INTERVAL);
            dialog.show(getSupportFragmentManager(), dialog.getClass().toString());
        }
    };

    @Override
    public void onTimeIntervalSelected(int requestCode, TimeIntervalFactory.TimeInterval interval) {
        switch (requestCode) {
            case REPEAT_TIME_INTERVAL:
                repeatTimeIntervalInput.setText(interval.toString());
                reminder.setRepeatTimeInterval(interval.getValue());
                break;
            case NOTIFICATION_TIME_INTERVAL:
                notificationTimeInput.setText(interval.toString());
                reminder.setEarlyWarningInterval(interval.getValue());
        }
    }

    private void populateFields() {
        titleInput.setText(reminder.getTitle());
        detailsInput.setText(reminder.getDescription());

        String dateDeadline = reminder.getDate(getString(R.string.date_format));
        String mileageDeadline = String.valueOf(reminder.getMileage());
        dateDeadlineInput.setText(dateDeadline);
        mileageDeadlineInput.setText(mileageDeadline);

        String repeatTime = timeIntervalFactory.from(reminder.getRepeatTimeInterval()).toString();
        String repeatMileage = String.valueOf(reminder.getRepeatMileage());
        repeatTimeIntervalInput.setText(repeatTime);
        repeatMileageIntervalInput.setText(repeatMileage);

        String notificationTime = timeIntervalFactory.from(reminder.getEarlyWarningInterval()).toString();
        String notificationMileage = String.valueOf(reminder.getEarlyWarningMileage());
        notificationTimeInput.setText(notificationTime);
        notificationMileageInput.setText(notificationMileage);
    }
}
