package com.sianadev.car_alerts.reminder.entity;

import com.sianadev.car_alerts.reminder.helper.TimeOperations;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "reminders")
public class Reminder implements Serializable, EstimableAlarmInterface {
    public static final int TYPE_CUSTOM = 0;
    public static final int TYPE_TIMING_BELT = 1;
    public static final int TYPE_OIL_CHANGE = 2;
    public static final int TYPE_INSURANCE = 3;
    public static final int TYPE_TECHNICAL_INSPECTION = 4;
    public static final int TYPE_MEDICAL_KIT = 5;
    public static final int TYPE_VIGNETTE = 6;
    public static final int TYPE_EXTINGUISHER = 7;
    public static final int TYPE_SPARK_PLUGS = 8;
    public static final int TYPE_ANTIFREEZE = 9;
    public static final int TYPE_BRAKE_FLUID = 10;

    @PrimaryKey(autoGenerate = true)
    private int id;
    private int vehicleId;
    private String title;
    private String description;
    private int type;
    private boolean onDate;
    private boolean onMileage;
    private Date date;
    private int mileage;
    private String earlyWarningInterval = "1 d";
    private Date earlyWarningDate;
    private int earlyWarningMileage = 100;
    private boolean repeating;
    private int repeatMileage;
    private String repeatTimeInterval = "";
    private boolean active;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isOnDate() {
        return onDate;
    }

    public void setOnDate(boolean onDate) {
        this.onDate = onDate;
    }

    public boolean isOnMileage() {
        return onMileage;
    }

    public void setOnMileage(boolean onMileage) {
        this.onMileage = onMileage;
    }

    public boolean hasBeenSet() {
        return isActive() && (onDate || onMileage);
    }

    @Override
    public Date getDate() {
        return date;
    }

    public String getDate(String format) {
        return new SimpleDateFormat(format, Locale.getDefault()).format(date);
    }

    public void setDate(Date date) {
        this.date = date;
        updateEarlyWarningDate();
    }

    @Override
    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    public String getEarlyWarningInterval() {
        return earlyWarningInterval;
    }

    public void setEarlyWarningInterval(String earlyWarningInterval) {
        this.earlyWarningInterval = earlyWarningInterval;
        updateEarlyWarningDate();
    }

    public int getEarlyWarningMileage() {
        return earlyWarningMileage;
    }

    public void setEarlyWarningMileage(int earlyWarningMileage) {
        this.earlyWarningMileage = earlyWarningMileage;
    }

    public boolean isRepeating() {
        return repeating;
    }

    public void setRepeating(boolean repeating) {
        this.repeating = repeating;
    }

    public int getRepeatMileage() {
        return repeatMileage;
    }

    public void setRepeatMileage(int repeatMileage) {
        this.repeatMileage = repeatMileage;
    }

    public String getRepeatTimeInterval() {
        return repeatTimeInterval;
    }

    public void setRepeatTimeInterval(String repeatTimeInterval) {
        this.repeatTimeInterval = repeatTimeInterval;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    private void updateEarlyWarningDate() {
        if (date != null && earlyWarningInterval != null) {
            earlyWarningDate = TimeOperations.substract(date, earlyWarningInterval);
        }
    }

    public Date getEarlyWarningDate() {
        return earlyWarningDate;
    }

    public void setEarlyWarningDate(Date earlyWarningDate) {
        this.earlyWarningDate = earlyWarningDate;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
