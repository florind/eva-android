package com.sianadev.car_alerts.reminder.config;

import com.sianadev.car_alerts.R;
import com.sianadev.car_alerts.reminder.entity.Reminder;
import com.sianadev.car_alerts.reminder.helper.TimeIntervalFactory;

public enum ReminderLayoutConfig {
    TIMING_BELT_CONFIG(R.layout.mileage_date_reminder, 50000, 1000, "5 y", "1 m"),
    OIL_CHANGE_CONFIG(R.layout.mileage_date_reminder, 15000, 200, "1 y", "14 d"),
    SPARK_PLUGS_CONFIG(R.layout.mileage_date_reminder, 30000, 100, "5 y", "14 d"),
    ANTIFREEZE_CONFIG(R.layout.mileage_date_reminder, 30000, 100, "10 y", "14 d"),
    BRAKE_FLUID_CONFIG(R.layout.mileage_date_reminder, 20000, 500, "2 y", "14 d"),
    INSURANCE_CONFIG(R.layout.repeatable_date_reminder, "1 y", "7 d"),
    TECHNICAL_INSPECTION_CONFIG(R.layout.repeatable_date_reminder, "1 y", "14 d"),
    VIGNETTE_CONFIG(R.layout.repeatable_date_reminder, "1 y", "7 d"),
    MEDICAL_KIT_CONFIG(R.layout.date_reminder, "7 d"),
    EXTINGUISHER_CONFIG(R.layout.date_reminder, "7 d"),
    CUSTOM_CONFIG(R.layout.activity_custom_reminder, 0, 0, "1 m", "7 d");

    private int layout;
    private int repeatMileage;
    private int notificationMileage;
    private TimeIntervalFactory.TimeInterval repeatTimeInterval;
    private TimeIntervalFactory.TimeInterval notificationTimeInterval;

    public static ReminderLayoutConfig instance(int type) {
        switch (type) {
            case Reminder.TYPE_TIMING_BELT : return TIMING_BELT_CONFIG;
            case Reminder.TYPE_OIL_CHANGE : return OIL_CHANGE_CONFIG;
            case Reminder.TYPE_SPARK_PLUGS : return SPARK_PLUGS_CONFIG;
            case Reminder.TYPE_ANTIFREEZE : return ANTIFREEZE_CONFIG;
            case Reminder.TYPE_BRAKE_FLUID : return BRAKE_FLUID_CONFIG;
            case Reminder.TYPE_INSURANCE: return INSURANCE_CONFIG;
            case Reminder.TYPE_TECHNICAL_INSPECTION: return TECHNICAL_INSPECTION_CONFIG;
            case Reminder.TYPE_VIGNETTE: return VIGNETTE_CONFIG;
            case Reminder.TYPE_MEDICAL_KIT: return MEDICAL_KIT_CONFIG;
            case Reminder.TYPE_EXTINGUISHER: return EXTINGUISHER_CONFIG;
            default: return CUSTOM_CONFIG;
        }
    }

    ReminderLayoutConfig(int layout, int repeatMileage, int notificationMileage, String repeatTimeInterval, String notificationTimeInterval) {
        this.layout = layout;
        this.repeatMileage = repeatMileage;
        this.notificationMileage = notificationMileage;
        this.repeatTimeInterval = TimeIntervalFactory.instance().from(repeatTimeInterval);
        this.notificationTimeInterval= TimeIntervalFactory.instance().from(notificationTimeInterval);
    }

    ReminderLayoutConfig(int layout, String repeatTimeInterval, String notificationTimeInterval) {
        this.layout = layout;
        this.repeatTimeInterval = TimeIntervalFactory.instance().from(repeatTimeInterval);
        this.notificationTimeInterval = TimeIntervalFactory.instance().from(notificationTimeInterval);
    }

    ReminderLayoutConfig(int layout, String notificationTimeInterval) {
        this.layout = layout;
        this.notificationTimeInterval = TimeIntervalFactory.instance().from(notificationTimeInterval);
    }

    public int getLayout() {
        return layout;
    }

    public String getRepeatMileage() {
        return String.valueOf(repeatMileage);
    }

    public TimeIntervalFactory.TimeInterval getRepeatTimeInterval() {
        return repeatTimeInterval;
    }

    public String getNotificationMileage() {
        return String.valueOf(notificationMileage);
    }

    public TimeIntervalFactory.TimeInterval getNotificationTimeInterval() {
        return notificationTimeInterval;
    }
}
