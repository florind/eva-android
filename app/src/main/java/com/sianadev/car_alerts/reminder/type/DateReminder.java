package com.sianadev.car_alerts.reminder.type;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.sianadev.car_alerts.R;
import com.sianadev.car_alerts.reminder.config.ReminderLayoutConfig;
import com.sianadev.car_alerts.reminder.entity.Reminder;
import com.sianadev.car_alerts.reminder.fragment.TimeIntervalPickerDialog;
import com.sianadev.car_alerts.reminder.helper.TimeIntervalFactory;
import com.sianadev.car_alerts.reminder.repository.ReminderRepository;
import com.sianadev.car_alerts.vehicle.entity.Vehicle;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;

public class DateReminder extends AppCompatActivity implements TimeIntervalPickerDialog.Listener {

    private static final int NOTIFICATION_TIME_INTERVAL = 470;

    private TextInputLayout titleLayout, detailsLayout,
            dateDeadlineLayout, notificationTimeLayout;

    private TextInputEditText titleInput, detailsInput,
            dateDeadlineInput, notificationTimeInput;

    private Calendar calendar;
    private Reminder reminder;
    private ReminderRepository repository;
    private TimeIntervalFactory timeIntervalFactory;
    private ReminderLayoutConfig layoutConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialize();

        setTitle(reminder.getTitle());
        setContentView(layoutConfig.getLayout());

        calendar = Calendar.getInstance();
        repository = new ReminderRepository(getApplicationContext());
        timeIntervalFactory = TimeIntervalFactory.instance();

        titleLayout = findViewById(R.id.title_layout);
        detailsLayout = findViewById(R.id.details_layout);
        dateDeadlineLayout = findViewById(R.id.date_deadline_layout);
        notificationTimeLayout = findViewById(R.id.notification_time_layout);

        titleInput = findViewById(R.id.title);
        detailsInput = findViewById(R.id.details);
        dateDeadlineInput = findViewById(R.id.date_deadline);
        notificationTimeInput = findViewById(R.id.notification_time);

        dateDeadlineInput.setOnClickListener(datepickerListener);
        notificationTimeInput.setOnClickListener(notificationTimeIntervalListener);

        if (reminder.getId() == 0) {
            setupDefaultFieldValues();
        } else {
            populateFields();
        }
    }

    private void setupDefaultFieldValues() {
        titleInput.setText(reminder.getTitle());
        notificationTimeInput.setText(layoutConfig.getNotificationTimeInterval().toString());
    }

    private void initialize() {
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            reminder = (Reminder) extras.getSerializable("REMINDER");
            Vehicle vehicle = (Vehicle) extras.getSerializable("VEHICLE");

            if (reminder == null || vehicle == null) { finish(); }

            reminder.setVehicleId(vehicle.getId());
            layoutConfig = ReminderLayoutConfig.instance(reminder.getType());

            if (reminder.getId() == 0) {
                reminder.setEarlyWarningInterval(layoutConfig.getNotificationTimeInterval().getValue());
            }
        } else {
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent response = new Intent();
            setResult(Activity.RESULT_CANCELED, response);
            finish();
            return true;
        }

        if (item.getItemId() == R.id.action_save) {
            if (saveReminder()) {
                Intent response = new Intent();
                setResult(Activity.RESULT_OK, response);
                finish();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean saveReminder() {
        if (reminderIsValid()) {
            reminder.setTitle(titleInput.getText().toString().trim());
            reminder.setDescription(detailsInput.getText().toString().trim());
            reminder.setOnDate(true);
            reminder.setOnMileage(false);
            reminder.setDate(calendar.getTime());
            reminder.setRepeating(false);

            reminder.setActive(true);
            repository.save(reminder);
            return true;
        }

        return false;
    }

    private boolean reminderIsValid() {
        boolean valid = true;

        if (titleInput.getText() == null || titleInput.getText().toString().trim().isEmpty()) {
            titleLayout.setError(getString(R.string.error_required));
            valid = false;
        }

        if (dateDeadlineInput.getText() == null || dateDeadlineInput.getText().toString().trim().isEmpty()) {
            dateDeadlineLayout.setError(getString(R.string.error_required));
            valid = false;
        }

        if (notificationTimeInput.getText() == null || notificationTimeInput.getText().toString().trim().isEmpty()) {
            notificationTimeLayout.setError(getString(R.string.error_required));
            valid = false;
        }

        return valid;
    }

    private View.OnClickListener datepickerListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            DatePickerDialog dialog = new DatePickerDialog(
                    view.getContext(),
                    R.style.Datepicker,
                    datepickerChangedListener,
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)
            );
            dialog.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis());
            dialog.show();
        }
    };

    private DatePickerDialog.OnDateSetListener datepickerChangedListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, day);

            SimpleDateFormat dateFormatter = new SimpleDateFormat(getString(R.string.date_format), Locale.getDefault());
            dateDeadlineInput.setText(dateFormatter.format(calendar.getTime()));
        }
    };

    private View.OnClickListener notificationTimeIntervalListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            TimeIntervalPickerDialog dialog = TimeIntervalPickerDialog.instance(getString(R.string.reminder_dialog_time_repeat_title), NOTIFICATION_TIME_INTERVAL);
            dialog.show(getSupportFragmentManager(), dialog.getClass().toString());
        }
    };

    @Override
    public void onTimeIntervalSelected(int requestCode, TimeIntervalFactory.TimeInterval interval) {
        if (requestCode == NOTIFICATION_TIME_INTERVAL ) {
            notificationTimeInput.setText(interval.toString());
            reminder.setEarlyWarningInterval(interval.getValue());
        }
    }

    private void populateFields() {
        titleInput.setText(reminder.getTitle());
        detailsInput.setText(reminder.getDescription());

        String dateDeadline = reminder.getDate(getString(R.string.date_format));
        dateDeadlineInput.setText(dateDeadline);

        String notificationTime = timeIntervalFactory.from(reminder.getEarlyWarningInterval()).toString();
        notificationTimeInput.setText(notificationTime);
    }
}
