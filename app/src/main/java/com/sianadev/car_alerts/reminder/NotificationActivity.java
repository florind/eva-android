package com.sianadev.car_alerts.reminder;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sianadev.car_alerts.R;
import com.sianadev.car_alerts.reminder.entity.Reminder;
import com.sianadev.car_alerts.reminder.fragment.NotificationConfirmDialog;
import com.sianadev.car_alerts.reminder.helper.AlarmEstimator;
import com.sianadev.car_alerts.reminder.helper.TimeOperations;
import com.sianadev.car_alerts.reminder.model.ReminderViewModel;
import com.sianadev.car_alerts.reminder.repository.ReminderRepository;
import com.sianadev.car_alerts.vehicle.entity.Vehicle;
import com.sianadev.car_alerts.vehicle.model.VehicleViewModel;

import java.util.Date;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

public class NotificationActivity extends AppCompatActivity implements NotificationConfirmDialog.Listener {
    private ReminderRepository alarmRepository;
    private Reminder alarm;
    private TextView entityTitle;
    private TextView alarmTitle;
    private TextView alarmEstimate;
    private TextView alarmDescription;
    private int mileage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_notification);

        entityTitle = findViewById(R.id.entity_title);
        alarmTitle = findViewById(R.id.alarm_title);
        alarmEstimate = findViewById(R.id.alarm_eta);
        alarmDescription = findViewById(R.id.alarm_description);
        Button confirmButton = findViewById(R.id.confirm);
        confirmButton.setOnClickListener(onConfirmClickListener);

        alarmRepository = new ReminderRepository(getApplication());
        handleIntent();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    private void handleIntent() {
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            int alarmId = extras.getInt("ALARM_ID", 0);

            if (alarmId == 0) {
                finish();
            }

            ReminderViewModel alarmViewModel = ViewModelProviders.of(this).get(ReminderViewModel.class);
            alarmViewModel.get(alarmId).observe(this, alarmObserver);
        } else {
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            setResult(Activity.RESULT_CANCELED, new Intent());
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private Observer<Reminder> alarmObserver = new Observer<Reminder>() {
        @Override
        public void onChanged(@Nullable Reminder foundAlarm) {
            if (foundAlarm != null) {
                alarm = foundAlarm;
                VehicleViewModel vehicleViewModel = ViewModelProviders.of(NotificationActivity.this).get(VehicleViewModel.class);
                vehicleViewModel.get(alarm.getVehicleId()).observe(NotificationActivity.this, vehicleObserver);
            }
        }
    };

    private Observer<Vehicle> vehicleObserver = new Observer<Vehicle>() {
        @Override
        public void onChanged(@Nullable Vehicle vehicle) {
            if (vehicle == null) {
                return;
            }

            mileage = vehicle.getMileage();
            entityTitle.setText(vehicle.getModel());
            alarmTitle.setText(alarm.getTitle());
            alarmDescription.setText(alarm.getDescription());

            AlarmEstimator estimator = new AlarmEstimator(getApplicationContext());
            estimator.setCurrentVehicleMileage(mileage);
            alarmEstimate.setText(estimator.estimate(alarm));
        }
    };

    private View.OnClickListener onConfirmClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (alarm.isRepeating()) {
                NotificationConfirmDialog dialog = new NotificationConfirmDialog();

                if (alarm.isOnDate()) {
                    dialog.requireDate();
                }

                if (alarm.isOnMileage()) {
                    dialog.requireMileage();
                    dialog.setCurrentMileage(mileage);
                }

                dialog.show(getSupportFragmentManager(), dialog.getClass().toString());
            } else {
                alarmRepository.delete(alarm);
                finish();
            }
        }
    };

    @Override
    public void onNotificationConfirm(Date date, int mileage) {
        if (!alarm.isRepeating()) {
            return;
        }

        if (alarm.isOnDate()) {
            alarm.setDate(TimeOperations.add(date, alarm.getRepeatTimeInterval()));
        }

        if (alarm.isOnMileage()) {
            alarm.setMileage(mileage + alarm.getRepeatMileage());
        }

        alarmRepository.save(alarm);
        finish();
    }
}
