package com.sianadev.car_alerts.reminder.helper;

import com.sianadev.car_alerts.R;
import com.sianadev.car_alerts.main.App;

import java.util.Arrays;

public class TimeIntervalFactory {
    private String[] singular;
    private String[] plural;
    private String[] map;

    TimeIntervalFactory(String[] singular, String[] plural, String[] map) {
        this.singular = singular;
        this.plural = plural;
        this.map = map;
    }

    public static TimeIntervalFactory instance() {
        return new TimeIntervalFactory(
            App.resources().getStringArray(R.array.time_interval_display_values),
            App.resources().getStringArray(R.array.time_interval_display_values_plural),
            App.resources().getStringArray(R.array.time_interval_value_mapping)
        );
    }

    public TimeInterval build(int value, int position) {
        String type = value > 1 ? plural[position] : singular[position];
        String displayText = String.format("%s %s", value, type);
        String displayValue = String.format("%s %s", value, map[position]);

        return new TimeInterval(displayText, displayValue);
    }

    public TimeInterval from(String interval) {
        if (interval == null || interval.trim().isEmpty()) {
            return new TimeInterval("", "");
        }

        String[] parts = interval.split(" ");
        int value = Integer.parseInt(parts[0]);
        String type = parts[1];

        int position = Arrays.asList(map).indexOf(type);

        return build(value, position);
    }

    public String[] getSingular() {
        return singular;
    }

    public String[] getPlural() {
        return plural;
    }

    public class TimeInterval {
        private String displayText;
        private String value;

        TimeInterval(String displayText, String value) {
            this.displayText = displayText;
            this.value = value;
        }

        public String getValue(){
            return value;
        }

        @Override
        public String toString() {
            return displayText;
        }
    }
}
