package com.sianadev.car_alerts.reminder.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;

import com.sianadev.car_alerts.R;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class NotificationService {
    private static final int NOTIFICATION_ICON = R.drawable.car_24dp;

    private String channelId;
    private int notificationId;
    private Context context;

    public NotificationService(Context context) {
        channelId = context.getString(R.string.channel_id);
        this.context = context;
    }

    public void build(String title, String text, String details, PendingIntent pendingIntent, PendingIntent deleteIntent) {
        createChannel(context);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, channelId);
        builder.setSmallIcon(NOTIFICATION_ICON)
                .setContentTitle(title)
                .setContentText(text)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setCategory(NotificationCompat.CATEGORY_REMINDER)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        if (details != null) {
            builder.setStyle(new NotificationCompat.BigTextStyle().bigText(details));
        }

        if (deleteIntent != null) {
            builder.setDeleteIntent(deleteIntent);
        }

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(nextNotificationId(), builder.build());
    }

    private void createChannel(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = context.getString(R.string.channel_name);
            String description = context.getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel channel = new NotificationChannel(channelId, name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private int nextNotificationId() {
        notificationId += 1;
        return notificationId;
    }
}
