package com.sianadev.car_alerts.reminder.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.sianadev.car_alerts.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public class NotificationConfirmDialog extends DialogFragment {
    private Listener listener;
    private Calendar calendar;
    private TextInputLayout dateInputLayout;
    private TextInputLayout mileageInputLayout;
    private TextInputEditText dateInput;
    private TextInputEditText mileageInput;
    private boolean requiresDate = false;
    private boolean requiresMileage = false;
    private int currentMileage;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        View layout = getActivity().getLayoutInflater().inflate(R.layout.notification_confirm_dialog, null);

        calendar = Calendar.getInstance();
        dateInputLayout = layout.findViewById(R.id.date_input_layout);
        mileageInputLayout = layout.findViewById(R.id.mileage_input_layout);
        mileageInput = layout.findViewById(R.id.mileage);
        mileageInput.setOnFocusChangeListener(onMileageFocusChangeListener);
        dateInput = layout.findViewById(R.id.date);
        dateInput.setOnClickListener(onDateInputClickListener);

        if (requiresDate) {
            dateInputLayout.setVisibility(View.VISIBLE);
        }

        if (requiresMileage) {
            mileageInputLayout.setVisibility(View.VISIBLE);
            mileageInputLayout.setHelperText(getString(R.string.confirm_notification_mileage_caption, String.valueOf(currentMileage)));
        }

        final AlertDialog dialog = builder.setView(layout)
                .setTitle(getString(R.string.confirm_notification_dialog_title))
                .setPositiveButton(getString(R.string.action_save), null)
                .setNegativeButton(R.string.action_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (formIsValid()) {
                            Date date = calendar.getTime();
                            int mileage = 0;
                            if (mileageInput.getText() != null) {
                                 if (!mileageInput.getText().toString().isEmpty()) {
                                     mileage = Integer.parseInt(mileageInput.getText().toString());
                                 }
                            }
                            listener.onNotificationConfirm(date, mileage);
                            dialog.dismiss();
                        }
                    }
                });
            }
        });

        return dialog;
    }

    private boolean formIsValid() {
        boolean valid = true;

        if (requiresDate && (dateInput.getText() == null || dateInput.getText().toString().trim().isEmpty())) {
            dateInputLayout.setError(getString(R.string.error_required));
            valid = false;
        }

        if (requiresMileage && (mileageInput.getText() == null || mileageInput.getText().toString().trim().isEmpty() || Integer.parseInt(mileageInput.getText().toString()) == 0)) {
            mileageInputLayout.setError(getString(R.string.error_required));
            valid = false;
        }

        return valid;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (Listener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException("Listener must implement interface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public interface Listener {
        void onNotificationConfirm(Date date, int mileage);
    }

    private View.OnClickListener onDateInputClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            dateInputLayout.setError(null);
            new DatePickerDialog(
                    view.getContext(),
                    R.style.Datepicker,
                    onDateSetListener,
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)
            ).show();
        }
    };

    private View.OnFocusChangeListener onMileageFocusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean hasFocus) {
            if (hasFocus) {
                mileageInputLayout.setError(null);
            }
        }
    };

    private DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, day);
            SimpleDateFormat dateFormat = new SimpleDateFormat(getString(R.string.date_format), Locale.getDefault());
            dateInput.setText(dateFormat.format(calendar.getTime()));
        }
    };

    public void requireDate() {
        requiresDate = true;
    }

    public void requireMileage() {
        requiresMileage = true;
    }

    public void setCurrentMileage(int mileage) {
        currentMileage = mileage;
    }
}
