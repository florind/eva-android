package com.sianadev.car_alerts.reminder.type;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.sianadev.car_alerts.R;
import com.sianadev.car_alerts.reminder.config.ReminderLayoutConfig;
import com.sianadev.car_alerts.reminder.entity.Reminder;
import com.sianadev.car_alerts.reminder.fragment.TimeIntervalPickerDialog;
import com.sianadev.car_alerts.reminder.helper.TimeIntervalFactory;
import com.sianadev.car_alerts.reminder.repository.ReminderRepository;
import com.sianadev.car_alerts.vehicle.entity.Vehicle;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;

public class CustomReminder extends AppCompatActivity implements TimeIntervalPickerDialog.Listener {

    private static final int REPEAT_TIME_INTERVAL = 835;
    private static final int NOTIFICATION_TIME_INTERVAL = 470;

    private TextInputLayout titleLayout, detailsLayout,
            dateDeadlineLayout, mileageDeadlineLayout,
            repeatTimeIntervalLayout, repeatMileageIntervalLayout,
            notificationTimeLayout, notificationMileageLayout;

    private TextInputEditText titleInput, detailsInput,
            dateDeadlineInput, mileageDeadlineInput,
            repeatTimeIntervalInput, repeatMileageIntervalInput,
            notificationTimeInput, notificationMileageInput;

    private MaterialCardView deadlineSection, repeatSection, notificationSection;
    private Switch dateToggle, mileageToggle, repeatToggle;

    private Calendar calendar;
    private Reminder reminder;
    private Vehicle vehicle;
    private ReminderRepository repository;
    private ReminderLayoutConfig layoutConfig;
    private TimeIntervalFactory timeIntervalFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialize();

        setTitle(reminder.getTitle());
        setContentView(layoutConfig.getLayout());

        calendar = Calendar.getInstance();
        repository = new ReminderRepository(getApplicationContext());
        timeIntervalFactory = TimeIntervalFactory.instance();

        titleLayout = findViewById(R.id.title_layout);
        detailsLayout = findViewById(R.id.details_layout);
        dateDeadlineLayout = findViewById(R.id.date_deadline_layout);
        mileageDeadlineLayout = findViewById(R.id.mileage_deadline_layout);
        repeatTimeIntervalLayout = findViewById(R.id.time_interval_layout);
        repeatMileageIntervalLayout = findViewById(R.id.mileage_interval_layout);
        notificationTimeLayout = findViewById(R.id.notification_time_layout);
        notificationMileageLayout = findViewById(R.id.notification_mileage_layout);

        titleInput = findViewById(R.id.title);
        detailsInput = findViewById(R.id.details);
        dateDeadlineInput = findViewById(R.id.date_deadline);
        mileageDeadlineInput = findViewById(R.id.mileage_deadline);
        repeatTimeIntervalInput = findViewById(R.id.time_interval);
        repeatMileageIntervalInput = findViewById(R.id.mileage_interval);
        notificationTimeInput = findViewById(R.id.notification_time);
        notificationMileageInput = findViewById(R.id.notification_mileage);

        dateToggle = findViewById(R.id.date_reminder_toggle);
        mileageToggle = findViewById(R.id.mileage_reminder_toggle);
        repeatToggle = findViewById(R.id.repeat_reminder_toggle);

        deadlineSection = findViewById(R.id.deadline_section);
        notificationSection = findViewById(R.id.notification_section);
        repeatSection = findViewById(R.id.repeat_section);

        dateDeadlineInput.setOnClickListener(datepickerListener);
        repeatTimeIntervalInput.setOnClickListener(repeatTimeIntervalListener);
        notificationTimeInput.setOnClickListener(notificationTimeIntervalListener);
        dateToggle.setOnCheckedChangeListener(controlToggleListener);
        mileageToggle.setOnCheckedChangeListener(controlToggleListener);
        repeatToggle.setOnCheckedChangeListener(controlToggleListener);

        if (reminder.getId() != 0) {
            populateFields();
        }
    }

    private void initialize() {
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            vehicle = (Vehicle) extras.getSerializable("VEHICLE");
            reminder = (Reminder) extras.getSerializable("REMINDER");

            if (vehicle == null || reminder == null) {
                finish();
            }

            reminder.setVehicleId(vehicle.getId());
            layoutConfig = ReminderLayoutConfig.instance(reminder.getType());
        } else {
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent response = new Intent();
            setResult(Activity.RESULT_CANCELED, response);
            finish();
            return true;
        }

        if (item.getItemId() == R.id.action_save) {
            if (saveReminder()) {
                Intent response = new Intent();
                setResult(Activity.RESULT_OK, response);
                finish();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean saveReminder() {
        if (reminderIsValid()) {
            reminder.setTitle(titleInput.getText().toString().trim());
            reminder.setDescription(detailsInput.getText().toString().trim());
            reminder.setOnDate(dateToggle.isChecked());
            reminder.setOnMileage(mileageToggle.isChecked());

            if (dateToggle.isChecked()) {
                reminder.setDate(calendar.getTime());
            }

            if (mileageToggle.isChecked()) {
                reminder.setMileage(Integer.parseInt(mileageDeadlineInput.getText().toString()));
                reminder.setEarlyWarningMileage(Integer.parseInt(notificationMileageInput.getText().toString()));
            }

            if (repeatToggle.isChecked()) {
                reminder.setRepeating(repeatToggle.isChecked());
                if (mileageToggle.isChecked()) {
                    reminder.setRepeatMileage(Integer.parseInt(repeatMileageIntervalInput.getText().toString()));
                }
            }

            reminder.setActive(true);
            repository.save(reminder);
            return true;
        }

        return false;
    }

    private boolean reminderIsValid() {
        boolean valid = true;

        if (!dateToggle.isChecked() && !mileageToggle.isChecked()) {
            Toast.makeText(getApplicationContext(), "You must select at least one option (date or mileage)", Toast.LENGTH_LONG).show();
            return false;
        }

        if (titleInput.getText() == null || titleInput.getText().toString().trim().isEmpty()) {
            titleLayout.setError(getString(R.string.error_required));
            valid = false;
        }

        if (dateToggle.isChecked() && (dateDeadlineInput.getText() == null || dateDeadlineInput.getText().toString().trim().isEmpty())) {
            dateDeadlineLayout.setError(getString(R.string.error_required));
            valid = false;
        }

        if (mileageToggle.isChecked() && (mileageDeadlineInput.getText() == null || mileageDeadlineInput.getText().toString().isEmpty())) {
            mileageDeadlineLayout.setError(getString(R.string.error_required));
            valid = false;
        } else if (mileageToggle.isChecked() && Integer.parseInt(mileageDeadlineInput.getText().toString()) < vehicle.getMileage()) {
            mileageDeadlineLayout.setError(getString(R.string.error_mileage_too_low, String.valueOf(vehicle.getMileage())));
        }

        if (repeatToggle.isChecked() && dateToggle.isChecked() && (repeatTimeIntervalInput.getText() == null || repeatTimeIntervalInput.getText().toString().trim().isEmpty())) {
            repeatTimeIntervalLayout.setError(getString(R.string.error_required));
            valid = false;
        }

        if (repeatToggle.isChecked() && mileageToggle.isChecked() && (repeatMileageIntervalInput.getText() == null || repeatMileageIntervalInput.getText().toString().isEmpty())) {
            repeatMileageIntervalLayout.setError(getString(R.string.error_required));
            valid = false;
        }

        if (dateToggle.isChecked() && (notificationTimeInput.getText() == null || notificationTimeInput.getText().toString().trim().isEmpty())) {
            notificationTimeLayout.setError(getString(R.string.error_required));
            valid = false;
        }

        if (mileageToggle.isChecked() && (notificationMileageInput.getText() == null || notificationMileageInput.getText().toString().isEmpty())) {
            notificationMileageLayout.setError(getString(R.string.error_required));
            valid = false;
        }

        return valid;
    }

    public CompoundButton.OnCheckedChangeListener controlToggleListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
            if (dateToggle.isChecked() || mileageToggle.isChecked()) {
                deadlineSection.setVisibility(View.VISIBLE);
                notificationSection.setVisibility(View.VISIBLE);
                repeatSection.setVisibility(repeatToggle.isChecked() ? View.VISIBLE : View.GONE);
            } else {
                deadlineSection.setVisibility(View.GONE);
                notificationSection.setVisibility(View.GONE);
                repeatSection.setVisibility(View.GONE);
            }

            handleSectionInputVisibilityToggle(dateToggle.isChecked(), mileageToggle.isChecked());
        }
    };

    private void handleSectionInputVisibilityToggle(boolean dateChecked, boolean mileageChecked) {
        //deadline
        TextInputLayout.LayoutParams dateDeadlineLayoutParams = (TextInputLayout.LayoutParams) dateDeadlineLayout.getLayoutParams();
        TextInputLayout.LayoutParams mileageDeadlineLayoutParams = (TextInputLayout.LayoutParams) mileageDeadlineLayout.getLayoutParams();
        dateDeadlineLayoutParams.weight = dateChecked && mileageChecked ? 1.0f : 2.0f;
        mileageDeadlineLayoutParams.weight = dateChecked && mileageChecked ? 1.0f : 2.0f;
        dateDeadlineLayout.setLayoutParams(dateDeadlineLayoutParams);
        mileageDeadlineLayout.setLayoutParams(mileageDeadlineLayoutParams);
        dateDeadlineLayout.setVisibility(dateChecked ? View.VISIBLE : View.GONE);
        mileageDeadlineLayout.setVisibility(mileageChecked ? View.VISIBLE : View.GONE);

        //notification
        TextInputLayout.LayoutParams notificationTimeLayoutParams = (TextInputLayout.LayoutParams) notificationTimeLayout.getLayoutParams();
        TextInputLayout.LayoutParams notificationMileageLayoutParams = (TextInputLayout.LayoutParams) notificationMileageLayout.getLayoutParams();
        notificationTimeLayoutParams.weight = dateChecked && mileageChecked ? 1.0f : 2.0f;
        notificationMileageLayoutParams.weight = dateChecked && mileageChecked ? 1.0f : 2.0f;
        notificationTimeLayout.setLayoutParams(notificationTimeLayoutParams);
        notificationMileageLayout.setLayoutParams(notificationMileageLayoutParams);
        notificationTimeLayout.setVisibility(dateChecked ? View.VISIBLE : View.GONE);
        notificationMileageLayout.setVisibility(mileageChecked ? View.VISIBLE : View.GONE);

        //repeat
        TextInputLayout.LayoutParams repeatTimeLayoutParams = (TextInputLayout.LayoutParams) repeatTimeIntervalLayout.getLayoutParams();
        TextInputLayout.LayoutParams repeatMileageLayoutParams = (TextInputLayout.LayoutParams) repeatMileageIntervalLayout.getLayoutParams();
        repeatTimeLayoutParams.weight = dateChecked && mileageChecked ? 1.0f : 2.0f;
        repeatMileageLayoutParams.weight = dateChecked && mileageChecked ? 1.0f : 2.0f;
        repeatTimeIntervalLayout.setLayoutParams(repeatTimeLayoutParams);
        repeatMileageIntervalLayout.setLayoutParams(repeatMileageLayoutParams);
        repeatTimeIntervalLayout.setVisibility(dateChecked ? View.VISIBLE : View.GONE);
        repeatMileageIntervalLayout.setVisibility(mileageChecked ? View.VISIBLE : View.GONE);
    }

    private View.OnClickListener datepickerListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            DatePickerDialog dialog = new DatePickerDialog(
                    view.getContext(),
                    R.style.Datepicker,
                    datepickerChangedListener,
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)
            );
            dialog.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis());
            dialog.show();
        }
    };

    private DatePickerDialog.OnDateSetListener datepickerChangedListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, day);

            SimpleDateFormat dateFormatter = new SimpleDateFormat(getString(R.string.date_format), Locale.getDefault());
            dateDeadlineInput.setText(dateFormatter.format(calendar.getTime()));
        }
    };

    private View.OnClickListener repeatTimeIntervalListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            TimeIntervalPickerDialog dialog = TimeIntervalPickerDialog.instance(getString(R.string.reminder_dialog_time_repeat_title), REPEAT_TIME_INTERVAL);
            dialog.show(getSupportFragmentManager(), dialog.getClass().toString());
        }
    };

    private View.OnClickListener notificationTimeIntervalListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            TimeIntervalPickerDialog dialog = TimeIntervalPickerDialog.instance(getString(R.string.reminder_dialog_time_repeat_title), NOTIFICATION_TIME_INTERVAL);
            dialog.show(getSupportFragmentManager(), dialog.getClass().toString());
        }
    };

    @Override
    public void onTimeIntervalSelected(int requestCode, TimeIntervalFactory.TimeInterval interval) {
        switch (requestCode) {
            case REPEAT_TIME_INTERVAL:
                repeatTimeIntervalInput.setText(interval.toString());
                reminder.setRepeatTimeInterval(interval.getValue());
                break;
            case NOTIFICATION_TIME_INTERVAL:
                notificationTimeInput.setText(interval.toString());
                reminder.setEarlyWarningInterval(interval.getValue());
        }
    }

    private void populateFields() {
        titleInput.setText(reminder.getTitle());
        detailsInput.setText(reminder.getDescription());

        dateToggle.setChecked(reminder.isOnDate());
        mileageToggle.setChecked(reminder.isOnMileage());
        repeatToggle.setChecked(reminder.isRepeating());

        if (reminder.isOnDate()) {
            String dateDeadline = reminder.getDate(getString(R.string.date_format));
            dateDeadlineInput.setText(dateDeadline);

            String notificationTime = timeIntervalFactory.from(reminder.getEarlyWarningInterval()).toString();
            notificationTimeInput.setText(notificationTime);

            if (reminder.isRepeating()) {
                String repeatTime = timeIntervalFactory.from(reminder.getRepeatTimeInterval()).toString();
                repeatTimeIntervalInput.setText(repeatTime);
            }
        }

        if (reminder.isOnMileage()) {
            String mileageDeadline = String.valueOf(reminder.getMileage());
            mileageDeadlineInput.setText(mileageDeadline);

            String notificationMileage = String.valueOf(reminder.getEarlyWarningMileage());
            notificationMileageInput.setText(notificationMileage);

            if (reminder.isRepeating()) {
                String repeatMileage = String.valueOf(reminder.getRepeatMileage());
                repeatMileageIntervalInput.setText(repeatMileage);
            }
        }
    }
}
