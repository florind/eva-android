package com.sianadev.car_alerts.reminder.work;

import android.content.Context;

import com.sianadev.car_alerts.R;
import com.sianadev.car_alerts.main.service.DatabaseService;
import com.sianadev.car_alerts.vehicle.entity.Vehicle;

import java.util.Calendar;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

public class PostponeMileageReadWorker extends Worker {
    public PostponeMileageReadWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        int vehicleId = getInputData().getInt("VEHICLE_ID", 0);

        if (vehicleId == 0) {
            return Result.SUCCESS;
        }

        Vehicle vehicle = DatabaseService.instance(getApplicationContext()).vehicleDao().syncGet(vehicleId);
        int days = getApplicationContext().getResources().getInteger(R.integer.conf_mileage_read_postpone_days);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 1);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.add(Calendar.DAY_OF_MONTH, days);

        if (vehicle == null) {
            return Result.SUCCESS;
        }

        vehicle.setNextMileageReadDate(calendar.getTime());
        DatabaseService.instance(getApplicationContext()).vehicleDao().save(vehicle);

        return Result.SUCCESS;
    }
}
