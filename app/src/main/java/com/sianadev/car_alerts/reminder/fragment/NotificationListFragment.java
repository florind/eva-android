package com.sianadev.car_alerts.reminder.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sianadev.car_alerts.R;
import com.sianadev.car_alerts.reminder.NotificationActivity;
import com.sianadev.car_alerts.reminder.adapter.NotificationListAdapter;
import com.sianadev.car_alerts.reminder.entity.AlarmNotification;
import com.sianadev.car_alerts.reminder.model.ReminderViewModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class NotificationListFragment extends Fragment implements NotificationListAdapter.Listener {
    private NotificationListAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = new NotificationListAdapter(this, getContext());
        ReminderViewModel notificationsList = ViewModelProviders.of(getActivity()).get(ReminderViewModel.class);

        notificationsList.getNotificationsLiveData().observe(this, new Observer<List<AlarmNotification>>() {
            @Override
            public void onChanged(@Nullable List<AlarmNotification> alarms) {
                adapter.setNotifications(alarms);
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.upcomming_alarm_list, container, false);

        // Set the adapter
        if (view.findViewById(R.id.list) instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = view.findViewById(R.id.list);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(adapter);
        }

        return view;
    }

    @Override
    public void onAlarmOptionsMenuClick(View view, AlarmNotification alarm) {
        Intent intent = new Intent(getContext(), NotificationActivity.class);
        intent.putExtra("ALARM_ID", alarm.getAlarmId());
        startActivity(intent);
    }
}
