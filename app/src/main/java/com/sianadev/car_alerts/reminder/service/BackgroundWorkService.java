package com.sianadev.car_alerts.reminder.service;

import android.content.Context;

import com.sianadev.car_alerts.R;
import com.sianadev.car_alerts.reminder.work.NotificationsWorker;

import java.util.concurrent.TimeUnit;

import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

public class BackgroundWorkService {
    private static final String VEHICLE_NOTIFICATIONS_WORK = "vehicle_notifications";

    public static void register(Context context) {
        int interval = context.getResources().getInteger(R.integer.conf_notification_check_interval);

        PeriodicWorkRequest.Builder builder = new PeriodicWorkRequest.Builder(
                NotificationsWorker.class, interval, TimeUnit.HOURS
        );

        PeriodicWorkRequest upcomingAlarmsNotifications = builder.build();

        WorkManager.getInstance().enqueueUniquePeriodicWork(
                VEHICLE_NOTIFICATIONS_WORK,
                ExistingPeriodicWorkPolicy.KEEP,
                upcomingAlarmsNotifications
        );
    }

    public static void unregister() {
        WorkManager.getInstance().cancelUniqueWork(VEHICLE_NOTIFICATIONS_WORK);
    }
}
