package com.sianadev.car_alerts.reminder.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import com.google.android.material.textfield.TextInputEditText;
import com.sianadev.car_alerts.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public class SingleTextInputFormDialog extends DialogFragment {
    private Listener listener;
    private int id;
    private TextInputEditText input;
    private String title;
    private String hint;
    private String value;
    private boolean multiline = false;

    public static SingleTextInputFormDialog instance(int id, String title, String text) {
        SingleTextInputFormDialog dialog = new SingleTextInputFormDialog();
        dialog.setId(id);
        dialog.setTitle(title);
        dialog.setHint(text);
        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getContext());

        View layout = getActivity().getLayoutInflater().inflate(R.layout.single_text_input_form, null);
        input = layout.findViewById(R.id.input);
        input.setHint(hint);

        if (value != null && !value.trim().isEmpty()) {
            input.setText(value);
        }

        if (multiline) {
            input.setLines(3);
            input.setMaxLines(3);
            input.setSingleLine(false);
            input.setGravity(Gravity.START);
            input.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
        }

        builder.setView(layout)
                .setTitle(title)
                .setPositiveButton(getString(R.string.action_save), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String text = input.getText() != null ? input.getText().toString() : "";
                        listener.onSubmitSingleTextInputForm(id, text);
                    }
                })
                .setNegativeButton(getString(R.string.action_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });;

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (Listener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException("Listener must implement interface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    private void setId(int id) {
        this.id = id;
    }

    private void setTitle(String text) {
        title = text;
    }

    private void setHint(String text) {
        hint = text;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setMultiline(boolean multiline) {
        this.multiline = multiline;
    }

    public interface Listener {
        void onSubmitSingleTextInputForm(int id, String value);
    }
}
