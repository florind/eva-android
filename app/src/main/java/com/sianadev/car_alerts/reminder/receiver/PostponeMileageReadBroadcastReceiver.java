package com.sianadev.car_alerts.reminder.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.sianadev.car_alerts.reminder.work.PostponeMileageReadWorker;

import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

public class PostponeMileageReadBroadcastReceiver extends BroadcastReceiver {
    public static final String ACTION = "POSTPONE_MILEAGE_READ";
    @Override
    public void onReceive(Context context, Intent intent) {
        if (!ACTION.equals(intent.getAction())) {
            return;
        }

        Bundle extras = intent.getExtras();
        if (extras == null) { return; }
        int vehicleId = extras.getInt("VEHICLE_ID", 0);
        if (vehicleId == 0) { return; }

        OneTimeWorkRequest postponeWork = new OneTimeWorkRequest.Builder(PostponeMileageReadWorker.class)
                .setInputData((new Data.Builder()).putInt("VEHICLE_ID", vehicleId).build())
                .build();
        String uniqueId = String.format("%s_%s", ACTION, String.valueOf(vehicleId));
        WorkManager.getInstance().enqueueUniqueWork(uniqueId, ExistingWorkPolicy.KEEP, postponeWork);
    }
}
